<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Lesson;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class Course extends Model
{
    protected $table = 'courses';
    protected $fillable = ['user_id'];

    public function user()
    {
        $user = User::where('id', '=', $this->user_id)->first();
        return $user;
    }


    public function category()
    {
        $category = Category::where('id', '=', $this->category_id)->first();
        return $category;
    }

    public function test()
    {
        $test = Test::where('id', '=', $this->test_id)->first();
        return $test;
    }

    public function term()
    {
        $term = Term::where('course_id', '=', $this->id)->first();
        return $term;
    }

    public function first_lesson()
    {
        $lesson = Lesson::where('course_id', '=', $this->id)->first();
        return $lesson;
    }

    public function last_lesson()
    {
        if (!Auth::user() || Cookie::get('last-lesson-' . $this->id . '-' . Auth::user()->id) == null) {

            $lessons = Lesson::where('course_id', '=', $this->id)->get();

            $result = $this->first_lesson();

            foreach ($lessons as $item) {
                if ($item->is_allowed() && $item->is_visited()) {
                    $result = $item;
                }
            }

            return $result;

        }
        else {
            $lesson = Lesson::where('id', '=', Cookie::get('last-lesson-' . $this->id . '-' . Auth::user()->id))->first();

            return $lesson;
        }
    }

    public function lessons()
    {
        $lessons = Lesson::where('course_id', '=', $this->id)->get();
        return $lessons;
    }
    

    public function progress($user_id = 0) {

        if ($user_id == 0) $user_id = Auth::user()->id;

        $progress = 0;

        $lessons = Lesson::where('course_id', '=', $this->id)->get();

        $lessons_count = 0;
        $visited_count = 0;

        $tests_count = 0;
        $passed_count = 0;
        foreach ($lessons as $item) {
            $lessons_count++;

            if ($item->is_visited($user_id) == 1) {
                $visited_count++;
            }

            if ($item->test_id > 0) {
                

                $test = Test::where('id', '=', $item->test_id)->first();
                if ($test) {

                    $tests_count++;

                    $attempts = Attempt::join('tests', 'tests.id', '=', 'attempts.test_id')
                        ->where('user_id', '=', $user_id)
                        ->where('test_id', '=', $test->id)
                        ->where('is_completed', '=', true)
                        ->select(['attempts.*', 'tests.title', 'tests.questions'])
                        ->orderBy('attempts.created_at')
                        ->get();

                    $best = 0;
                    foreach ($attempts as $attempt) {
                        if ($best < $attempt->correct_amount) $best = $attempt->correct_amount;
                    }

                    if ($best * 100 / $test->questions >= 80) {
                        $passed_count++;
                    }


                }

            }
        }

        if ($tests_count > 0) {
            $lessons_coef = 30;
            $tests_coef = 55;
        }
        else {
            $lessons_coef = 85;
            $tests_coef = 0;
        }

        if ($lessons_count > 0) $lessons_progress = floor($lessons_coef * $visited_count / $lessons_count); else $lessons_progress = 0;

        if ($tests_count > 0) $tests_progress = floor($tests_coef * $passed_count / $tests_count); else $tests_progress = 0;

        $progress = $progress +  $lessons_progress + $tests_progress;

        return $progress;
    }

    
}

