<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\QuestionOption;

class Question extends Model
{
    protected $table = 'questions';
    public $timestamps = false;

    public function options_count()
    {
        $count = QuestionOption::where('question_id', '=', $this->id)->count();
        return $count;
    }

}

