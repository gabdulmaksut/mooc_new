<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChatterPost extends Model
{
    use SoftDeletes;
    protected $table = 'chatter_post';

    public function user()
    {
        $user = User::where('id', '=', $this->user_id)->first();

        $result['status'] = 'ok';
        $result['info'] = $user;
        $result['color'] = '#' . mb_substr(dechex(crc32($user->email)), 0, 6);
        $result['first_letter'] = mb_substr( $user->firstname, 0, 1) . mb_substr( $user->lastname, 0, 1) ;

        return $result;
    }


    public function discussion()
    {
        $result = ChatterDiscussion::where('id', '=', $this->chatter_discussion_id)->first();
        return $result;
    }
}

