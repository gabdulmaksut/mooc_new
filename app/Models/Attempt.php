<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Answer;
use App\Models\Question;
use App\Models\QuestionOption;

class Attempt extends Model
{
    protected $table = 'attempts';

    public function calculate_correct()
    {
        $correct_amount = 0;

        $answers = Answer::where('attempt_id', '=', $this->id)
            ->get()->keyBy('number');

        foreach ($answers as $item)
        {
            $is_correct = true;

            $question = Question::where('id', '=', $item->question_id)->first();

            if ($item->answer != "") {
                if ($question->type == 1) {
                    $question_option = QuestionOption::where('id', '=', $item->answer)
                        ->first();

                    $is_correct = $question_option->is_correct;
                }

                if ($question->type == 2) {
                    $answers_array = explode(",", $item->answer);

                    $correct_options = QuestionOption::where('question_id', '=', $item->question_id)
                        ->where('is_correct', '=', true)
                        ->pluck('id')->toArray();

                    $is_correct = true;

                    foreach ($answers_array as $key) {
                        if (!in_array($key, $correct_options)) {
                            $is_correct = false;
                        }
                    }

                    if (count($answers_array) != count($correct_options)) $is_correct = false;

                }

                if ($question->type == 3) {
                    $question_option = QuestionOption::where('question_id', '=', $item->question_id)
                        ->first();

                    $is_correct = false;
                    if ($question_option->text == $item->answer) $is_correct = true;
                }


                if ($question->type == 4) {

                    $question_options =  QuestionOption::where('question_id', '=', $item->question_id)
                        ->where('is_correct', '=', 1)
                        ->select(['id', 'question_id', 'number', 'text', 'coordinates','correct_relation'])
                        ->get()->keyBy('number');

                    $answer_map = $item->answer;
                    if ($answer_map == '') {
                        foreach ($question_options as $item)
                        {
                            $answer_map[0][] = $item->number;
                        }
                    }
                    else {
                        $answer_map = json_decode((string)$answer_map);
                    }


                    foreach ($answer_map as $i => $row) {
                        foreach ($row as $j => $value) {
                            if ($question_options[$value]->correct_relation != $i) {
                                $is_correct = false;
                            }
                        }
                    }


                }

                if ($is_correct == true) $correct_amount++;
            }
        }

        $this->correct_amount = $correct_amount;
        $this->save();
    }
}

