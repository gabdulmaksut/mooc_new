<?php

namespace App\Models;
use App\Models\Course;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    public $timestamps = false;

    public function courses()
    {
        $courses = Course::all();
        return $courses;
    }
}

