<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Cache;
use App\Models\Chat;
use App\Models\Course;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    protected $appends = array('email');

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'password'
    ];

    public static $validation = [
        'id'    => 'unique:users',
        'password' => 'required|confirmed|min:1',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $querystring   = '';

    public function getAuthPassword()
    {
        return $this->password;
    }


    public function isOnline()
    {
        return Cache::has('user-is-online-' . $this->id);
    }
    public function courses() {
    return $this->has_many('App\Models\Course');
}

    public function unreadMessagesCount($from = 0)
    {
        $count = Chat::where('to', '=', Auth::user()->id);
        if ($from > 0) $count = $count->where('from', '=', $from);
        $count = $count->where('is_read', '=', 0)->count();
        if ($count > 99) $count = '99+';
        return $count;
    }

    public function color()
    {
        $result = '#' . mb_substr(dechex(crc32($this->email)), 0, 6);

        return $result;
    }


    public function first_letter()
    {
        $result = mb_substr( $this->firstname, 0, 1) . mb_substr( $this->lastname, 0, 1) ;

        return $result;
    }

    public function course()
    {
        $course = Course::where('user_id', '=', $this->id)->first();
        $this->set('course', $course);
    }

    public function is_teach($student_id) {
        $my_courses = Course::where('user_id', '=', $this->id)->pluck('id')->toArray();
        $my_students = MyCourses::whereIn('course_id', $my_courses)->pluck('user_id')->toArray();
        if (in_array($student_id, $my_students)) return true; else return false;
    }

}
