<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LessonVideo extends Model
{
    protected $fillable = ['lesson_id', 'filename'];
 
    public function product()
    {
        return $this->belongsTo('App\models\Lesson');
    }
}
