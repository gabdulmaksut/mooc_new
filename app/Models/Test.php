<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Lesson;

class Test extends Model
{
    protected $table = 'tests';
    public $timestamps = false;

    public function lesson()
    {
        $lesson = Lesson::where('test_id', '=', $this->id)->first();
        return $lesson;
    }

    public function attempts($user_id = 0)
    {
        $attempts = Attempt::where('test_id', '=', $this->id);
        if ($user_id > 0) $attempts = $attempts->where('user_id', '=', $user_id);
        $attempts = $attempts->get();
        return $attempts;
    }


}

