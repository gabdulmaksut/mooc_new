<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Course;
use App\Models\File;
use App\Models\Video;
use App\Models\Test;
use Illuminate\Support\Facades\Auth;

class Lesson extends Model
{
    protected $table = 'lessons';
    protected $fillable = ['course_id'];

    public function course()
    {
       //return $this->belongsTo('App\Models\Course', 'course_id');
        $course = Course::where('id', '=', $this->course_id)->first();
        return $course;
    }

    public function test()
    {
        $test = Test::where('id', '=', $this->test_id)->first();
        return $test;
    }

    public function files()
    {
        $files = File::where('lesson_id', '=', $this->id)->get();

        $result = [];
        $i = 0;
        foreach ($files as $file) {
            $result[$i]['name'] = $file->filename;
            $result[$i]['path'] = $file->path;
            $i++;
        }
        
        return $result;
    }   

    public function videos()
    {
        $videos = Video::where('lesson_id', '=', $this->id)->get();

        $result = [];
        $i = 0;
        foreach ($videos as $video) {
            $result[$i]['link'] = $video->link;
            $result[$i]['name'] = $video->name;
            $i++;
        }
        
        return $result;
    }

    public function is_allowed()
    {
        if (Auth::user()) {
            $prev_lessons = Lesson::where('course_id', '=', $this->course_id)->where('id', '<', $this->id)->get();

            $result = true;

            foreach ($prev_lessons as $item) {
                if ($item->test_id > 0) {
                    $test = Test::where('id', '=', $item->test_id)->first();

                    $attempts = Attempt::join('tests', 'tests.id', '=', 'attempts.test_id')
                        ->where('user_id', '=', Auth::user()->id)
                        ->where('test_id', '=', $test->id)
                        ->where('is_completed', '=', true)
                        ->select(['attempts.*', 'tests.title', 'tests.questions'])
                        ->orderBy('attempts.created_at')
                        ->get();

                    $best = 0;
                    foreach ($attempts as $attempt) {
                        if ($best < $attempt->correct_amount) $best = $attempt->correct_amount;
                    }

                    if ($best * 100 / $test->questions < 80) {
                        $result = false;
                    }
                }
            }

            return $result;
        }
        else return false;
    }

    public function is_visited($user_id = 0)
    {
        if (Auth::user()) {
            if ($user_id == 0) $user_id = Auth::user()->id;

            $visit = LessonsVisits::where('user_id','=', $user_id)->where('lesson_id', '=', $this->id)->first();

            if ($visit == null) return 0; else return 1;
        }

        return 0;
    }

    public function test_is_passed($user_id = 0) {

        if (Auth::user()) {
            if ($user_id == 0) $user_id = Auth::user()->id;

            $test = Test::where('id', '=', $this->test_id)->first();

            $attempts = Attempt::join('tests', 'tests.id', '=', 'attempts.test_id')
                ->where('user_id', '=', $user_id)
                ->where('test_id', '=', $test->id)
                ->where('is_completed', '=', true)
                ->select(['attempts.*', 'tests.title', 'tests.questions'])
                ->orderBy('attempts.created_at')
                ->get();

            $best = 0;
            foreach ($attempts as $attempt) {
                if ($best < $attempt->correct_amount) $best = $attempt->correct_amount;
            }

            if ($best * 100 / $test->questions >= 80) {
                return 1;
            }
        }
        return 0;

    }

}
