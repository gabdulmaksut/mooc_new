<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Input;

class StoreUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users,email,' . $this->segment(3),
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => transl('Не введен e-mail'),
            'email.unique' => transl('Пользователь с такой почтой уже существует'),
            'password.required' => transl('Не введен пароль'),
            'password_confirmation.required' => transl('Введите пароль еще раз'),
            'password.confirmed' => transl('Введенные пароли не совпадают'),
            'email.email' => transl('Неверный формат почты'),
        ];
    }
}
