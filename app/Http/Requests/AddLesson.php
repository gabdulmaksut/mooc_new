<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Input;

class AddLesson extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
            return [
            'lesson_name' => 'required'];
            //'docs' => 'image|mimes:jpeg,bmp,png|size:2000'];
            $docs = count($this->input('docs'));
            foreach(range(0, $docs) as $index) {
            $rules['docs.' . $index] = 
            'required|mimes:pdf,jpeg,bmp,png,doc,docx,csv,xlsx|max:500000';
             }
             return $rules;
    }

        public function messages()
    {
        return [
            'email.required' => transl('Не введен e-mail'),
            'email.unique' => transl('Пользователь с такой почтой уже существует'),
            'password.required' => transl('Не введен пароль'),
            'password_confirmation.required' => transl('Введите пароль еще раз'),
            'password.confirmed' => transl('Введенные пароли не совпадают'),
            'email.email' => transl('Неверный формат почты'),
        ];
    }
}
