<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {


        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect('auth/login');
            }
        }



        if (!Auth::user()->is_active) {
            Auth::logout();
            return redirect('auth/login')->with('error', transl("Аккаунт еще не активирован<br/><br/>Для активации аккаунта следуйте инструкции, высланной на вашу почту"));
        };

        return $next($request);
    }
}
