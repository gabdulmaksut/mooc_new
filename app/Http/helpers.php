<?php

function transl($id, $parameters = array(), $domain = 'messages', $locale = null)
{
    return $id;
    $filename = explode('.', $id);
    if (!$filename[0] || !in_array(lcfirst($filename[0]), ['messages', 'pagination', 'reminders', 'validation', 'models']))
    {
        $id = 'messages.' . $id;
    }
    return app('translator')->trans($id, $parameters, $domain, $locale);
}