<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App, Validator, DB, Hash, Cookie, Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Redirect;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use Storage;
use App\Models\User;
use App\Models\Course;
use App\Models\Lesson;


class HomeController extends Controller
{
    public function getIndex()
    {

    }
    public function postIndex()
    {

    }
}
