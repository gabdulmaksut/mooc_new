<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\Test;
use App\Models\Question;
use App\Models\QuestionOption;
use Illuminate\Support\Facades\Cookie;

class TestsListController extends Controller
{

    public function before()
    {
        parent::before();
        $this->section = 'tests-list';
        $this->set('_section', 'tests-list');
    }

    public function getIndex()
    {
        $tests = Test::where('is_free', '=', true)->get();

        $this->set('tests', $tests)->set('id', Cookie::get('user_id'));
    }



}