<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Request, App, Validator, Hash, Cookie;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\Models\Test;
use App\Models\Question;
use App\Models\QuestionOption;
use App\Models\Image;
use App\Models\Attempt;
use App\Models\Answer;
use App\Models\User;



class TestController extends Controller
{

    public function before()
    {
        parent::before();
        $this->section = 'admin-panel';
        $this->set('_section', 'welcome'); 
    }


    public function getIndex($test_id = 0)
    {
        $test = Test::where('id', '=', $test_id)->first();

        if ($test == null) {
            return redirect('/courses/mycourses');
        }

        if (Auth::user()) {
            $user_id = Auth::user()->id;
            $attempt = Attempt::where('user_id', '=', $user_id)
                ->where('test_id', '=', $test->id)
                ->where('is_completed', '=', false)
                ->orderBy('updated_at', 'desc')
                ->first();

            $attempts_count = Attempt::where('user_id', '=', $user_id)
                ->where('test_id', '=', $test->id)
                ->where('is_completed', '=', true)
                ->count();
        }
        else {
            $attempt = Attempt::where('cookie', '=', Cookie::get('user_id'))
                ->where('test_id', '=', $test->id)
                ->where('is_completed', '=', false)
                ->orderBy('updated_at', 'desc')
                ->first();

            $attempts_count = Attempt::where('cookie', '=', Cookie::get('user_id'))
                ->where('test_id', '=', $test->id)
                ->where('is_completed', '=', true)
                ->count();
        }

        if ($attempt != null && $attempt->is_completed == false) {

            if ( $test->time > 0) {
                $time_left = Carbon::now()->timestamp - Carbon::parse($attempt->created_at)->timestamp;

                if ($test->time * 60 - $time_left < 0) {
                    $attempt->is_completed = true;
                    $attempt->save();
                    $this->set('is_completed', true);
                }
            }

            $this->set('attempt', $attempt);
        }

        $this->set('test', $test)->set('attempts_count', $attempts_count)->set('embed', Input::get('embed', null));
    }

    public function getTest($test_id = 0, $question_index = 0)
    {
        $test = Test::where('id', '=', $test_id)->first();

        if ($test == null) {
            return redirect('/courses/mycourses');
        }

        if (Auth::user()) {
            $user_id = Auth::user()->id;
            $attempt = Attempt::where('user_id', '=', $user_id)
                ->where('test_id', '=', $test_id)
                ->where('is_completed', '=', false)
                ->orderBy('updated_at', 'desc')
                ->first();

            $attempts_count = Attempt::where('user_id', '=', $user_id)
                ->where('test_id', '=', $test->id)
                ->where('is_completed', '=', true)
                ->count();
        }
        else {
            $attempt = Attempt::where('cookie', '=', Cookie::get('user_id'))
                ->where('test_id', '=', $test_id)
                ->where('is_completed', '=', false)
                ->orderBy('updated_at', 'desc')
                ->first();

            $attempts_count = Attempt::where('cookie', '=', Cookie::get('user_id'))
                ->where('test_id', '=', $test->id)
                ->where('is_completed', '=', true)
                ->count();
        }

        if ($attempt == null || ($test->attempts > 0 && $test->attempts - $attempts_count <= 0)) {
            return redirect('/courses/mycourses');
        }

        if ($question_index < 1 || $question_index > $test->questions) {
            return redirect('/courses/mycourses');
        }

        $question_number = explode(";", $attempt->pattern)[$question_index - 1];
        $question = Question::where('test_id', '=', $test_id)
            ->where('number', '=', $question_number)
            ->first();

        $question_images = Image::where('test_id', '=', $test_id)
            ->where('coordinates', '=', $question->coordinates)
            ->get();

        $question_options = [];
        $question_cells = [];
        if ($question->type == 4) {
            $question_options =  QuestionOption::where('question_id', '=', $question->id)
                ->where('is_correct', '=', 1)
                ->select(['id', 'question_id', 'number', 'text', 'coordinates'])
                ->get()->keyBy('number');
            $question_cells =  QuestionOption::where('question_id', '=', $question->id)
                ->where('is_correct', '=', 0)
                ->select(['id', 'question_id', 'number', 'text', 'coordinates'])
                ->get()->keyBy('number');
        }
        else {
            $question_options = QuestionOption::where('question_id', '=', $question->id)
                ->select(['id', 'question_id', 'number', 'text', 'coordinates'])
                ->get()->keyBy('number');
        }

        $question_options_images = [];
        foreach ($question_options as $item)
        {
            $question_options_images[$item->id] = Image::where('test_id', '=', $test_id)
                ->where('coordinates', '=', $item->coordinates)
                ->get();
        }

        $question_cells_images = [];
        foreach ($question_cells as $item)
        {
            $question_cells_images[$item->id] = Image::where('test_id', '=', $test_id)
                ->where('coordinates', '=', $item->coordinates)
                ->get();
        }

        $answers = Answer::where('attempt_id', '=', $attempt->id)
            ->get()->keyBy('number');

        $answers_left = Answer::where('attempt_id', '=', $attempt->id)
            ->where('answer', '=', "")
            ->count();

        $last_answer = null;
        if ($answers_left == 1) {
            $last_answer = Answer::where('attempt_id', '=', $attempt->id)
                ->where('answer', '=', "")
                ->first();
        }

        $answer = Answer::where('attempt_id', '=', $attempt->id)
            ->where('question_id', '=', $question->id)
            ->first();

        $answer_map = null;
        if ($question->type == 4) {
            $answer_map = $answer->answer;
            if ($answer_map == '') {
                foreach ($question_options as $item)
                {
                    $answer_map[0][] = $item->number;
                }
            }
            else {
                $answer_map = json_decode((string)$answer_map);
            }
        }


        if ($test->time == 0) $time_left = 0;
        else {
            $time_left = Carbon::now()->timestamp - Carbon::parse($attempt->created_at)->timestamp;

            if ($test->time * 60 - $time_left < 0) {
                $time_left = $test->time * 60;

                $attempt->calculate_correct();

                $attempt->is_completed = true;
                $attempt->save();
            }
        }

        $this->set('test', $test)
            ->set('attempt', $attempt)
            ->set('question', $question)
            ->set('question_options', $question_options)
            ->set('question_cells', $question_cells)
            ->set('question_index', $question_index)
            ->set('question_images', $question_images)
            ->set('question_options_images', $question_options_images)
            ->set('question_cells_images', $question_cells_images)
            ->set('answer', $answer)
            ->set('answers', $answers)
            ->set('answers_left', $answers_left)
            ->set('last_answer', $last_answer)
            ->set('answer_map', $answer_map)
            ->set('time_left', $time_left)
            ->set('embed', Input::get('embed', null));
    }


    public function getResult($test_id = 0, $attempt_id = 0)
    {

        if (Auth::user() == null) {
            return redirect('auth/login')->withInput(Request::except('password'))
                ->with('login_error', transl('Войдите в личный кабинет, чтобы посмотреть результаты.'));
        }


        $attempt = Attempt::where('id', '=', $attempt_id)
            ->where('test_id', '=', $test_id)
            ->where('is_completed', '=', true)
            ->first();


        if ($attempt == null) {
            return redirect('/courses/mycourses');
        }


        if (Auth::user()->id == $attempt->user_id) {
            $attempt_number = Attempt::where('user_id', '=', Auth::user()->id)
                    ->where('test_id', '=', $test_id)
                    ->where('created_at', '<', $attempt->created_at)
                    ->count() + 1;
        }
        else {
            if (Auth::user()->role == 3 || (Auth::user()->role == 2 && Auth::user()->is_teach($attempt->user_id))) {

                $user = User::where('id', '=', $attempt->user_id)->first();
                $this->set('user', $user);

                $attempt_number = Attempt::where('user_id', '=', $attempt->user_id)
                        ->where('test_id', '=', $test_id)
                        ->where('created_at', '<', $attempt->created_at)
                        ->count() + 1;

            }
            else {
                return redirect('/courses/mycourses');
            }
        }


        $test = Test::where('id', '=', $test_id)->first();

        if ($test == null) {
            return redirect('/courses/mycourses');
        }

        $answers = Answer::where('attempt_id', '=', $attempt_id)
            ->get()->keyBy('number');

        $questions = [];
        $questions_images = [];
        $questions_options = [];
        $questions_cells = [];
        $questions_options_images = [];
        $questions_cells_images = [];

        $is_correct = [];
        $correct_count = 0;

        $answer_maps = [];
        $is_correct_cells = [];
        $is_correct_objects = [];

        foreach ($answers as $item)
        {
            $is_correct[$item->id] = true;

            $questions[$item->id] = Question::where('id', '=', $item->question_id)->first();

            $questions_images[$item->id] = Image::where('test_id', '=', $test_id)
                ->where('coordinates', '=', $questions[$item->id]->coordinates)
                ->get();

            if ($item->answer != "") {
                if ($questions[$item->id]->type == 1) {
                    $questions_options[$item->id] = QuestionOption::where('id', '=', $item->answer)
                        ->first();

                    $questions_options_images[$questions_options[$item->id]->id] = Image::where('test_id', '=', $test_id)
                        ->where('coordinates', '=', $questions_options[$item->id]->coordinates)
                        ->get();

                    $is_correct[$item->id] = $questions_options[$item->id]->is_correct;

                }

                if ($questions[$item->id]->type == 2) {
                    $answers_array = explode(",", $item->answer);

                    foreach ($answers_array as $key)
                    {
                        $questions_options[$item->id][$key] = QuestionOption::where('id', '=', $key)
                            ->first();

                        $questions_options_images[$questions_options[$item->id][$key]->id] = Image::where('test_id', '=', $test_id)
                            ->where('coordinates', '=', $questions_options[$item->id][$key]->coordinates)
                            ->get();

                    }

                    $correct_options = QuestionOption::where('question_id', '=', $item->question_id)
                        ->where('is_correct', '=', true)
                        ->pluck('id')->toArray();

                    $is_correct[$item->id] = true;

                    foreach ($answers_array as $key) {
                        if (!in_array($key, $correct_options)) {
                            $is_correct[$item->id] = false;
                        }
                    }

                    if (count($answers_array) != count($correct_options)) $is_correct[$item->id] = false;

                }

                if ($questions[$item->id]->type == 3) {
                    $questions_options[$item->id] = QuestionOption::where('question_id', '=', $item->question_id)
                        ->first();

                    $is_correct[$item->id] = false;
                    if ($questions_options[$item->id]->text == $item->answer) $is_correct[$item->id] = true;
                }

                if ($questions[$item->id]->type == 4) {

                    $questions_options[$item->id] =  QuestionOption::where('question_id', '=', $questions[$item->id]->id)
                        ->where('is_correct', '=', 1)
                        ->select(['id', 'question_id', 'number', 'text', 'coordinates','correct_relation'])
                        ->get()->keyBy('number');


                    foreach ($questions_options[$item->id] as $value)
                    {
                        $questions_options_images[$item->id][$value->id] = Image::where('test_id', '=', $test_id)
                            ->where('coordinates', '=', $value->coordinates)
                            ->get();
                    }

                    $questions_cells[$item->id] = QuestionOption::where('question_id', '=', $questions[$item->id]->id)
                        ->where('is_correct', '=', 0)
                        ->select(['id', 'question_id', 'number', 'text', 'coordinates'])
                        ->get()->keyBy('number');

                    foreach ($questions_cells[$item->id] as $value)
                    {
                        $questions_cells_images[$item->id][$value->id] = Image::where('test_id', '=', $test_id)
                            ->where('coordinates', '=', $value->coordinates)
                            ->get();
                    }

                    $answer_maps[$item->id] = $item->answer;
                    if ($answer_maps[$item->id] == '') {
                        foreach ($questions_options[$item->id] as $item)
                        {
                            $answer_maps[$item->id][0][] = $item->number;
                        }
                    }
                    else {
                        $answer_maps[$item->id] = json_decode((string)$answer_maps[$item->id]);
                    }


                    foreach ($answer_maps[$item->id] as $i => $row) {
                        if (!isset($is_correct_cells[$item->id][$i])) $is_correct_cells[$item->id][$i] = 1;
                        foreach ($row as $j => $value) {
                            if ($questions_options[$item->id][$value]->correct_relation != $i) {
                                $is_correct_cells[$item->id][$i] = 0;
                                $is_correct_cells[$item->id][$questions_options[$item->id][$value]->correct_relation] = 0;
                                $is_correct_objects[$item->id][$value] = 0;
                                $is_correct[$item->id] = false;
                            }
                            else {
                                $is_correct_objects[$item->id][$value] = 1;
                            }
                        }
                    }


                }

                if ($is_correct[$item->id] == true) $correct_count++;
            }


        }


        $this->set('test', $test)
            ->set('attempt', $attempt)
            ->set('answers', $answers)
            ->set('questions', $questions)
            ->set('questions_images', $questions_images)
            ->set('questions_options', $questions_options)
            ->set('questions_options_images', $questions_options_images)
            ->set('questions_cells_images', $questions_cells_images)
            ->set('is_correct', $is_correct)
            ->set('correct_count', $correct_count)
            ->set('attempt_number', $attempt_number)
            ->set('questions_cells', $questions_cells)
            ->set('answer_maps', $answer_maps)
            ->set('is_correct_cells', $is_correct_cells)
            ->set('is_correct_objects', $is_correct_objects)
            ->set('embed', Input::get('embed', null));
    }

}