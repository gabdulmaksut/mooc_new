<?php
namespace App\Http\Controllers;

use Request, Cookie;
use App\Models\User;
use App\Http\Requests\StoreUserRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Waavi\ReCaptcha\Facades\ReCaptcha;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

use App\Models\Attempt;

class AuthController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest', ['except' => ['getLogout']]);
    }

    public function before()
    {
        parent::before();
        $this->section = 'auth';
        $this->set('_section', 'auth');
    }

    public function getIndex()
    {
        return redirect('/');
    }

    public function getLogin()
    {

        if(Auth::user()) {
            Auth::logout();
        }

        session()->regenerate();

    }


    public function postLogin()
    {
        if (Auth::check()) {
            return redirect('/');
        }


		$remember = Request::input('remember', 0);
		$remember = boolval($remember);

		$check = User::where('email', '=', (string)Request::input('username'))->first();
		if (!$check) {
			return redirect('auth/login')->withInput(Request::except('password'))
				->with('login_error', transl('Неверный логин или пароль'));
		}

		if (Auth::attempt(['email' => Request::input('username'), 'password' => Request::input('password'), 'is_active' => 1], $remember)) {

            $cookie = Cookie::get('user_id');

            $attempts = Attempt::where('user_id', '=', $cookie)->get();

            foreach ($attempts as $attempt) {
                $attempt->user_id = Auth::user()->id;
                $attempt->save();
            }
		    //return redirect()->intended('/courses/allcourses/');
            return redirect()->intended('courses/mycourses/');
		}

		if (Auth::attempt(['email' => Request::input('username'), 'password' => Request::input('password'), 'is_active' => 0], $remember)) {
			Auth::logout();
			return redirect('auth/login')->withInput(Request::except('password'))
				->with('login_error', transl('Аккаунт еще не активирован<br/><br/>На вашу почту было выслано письмо c инструкцией для активации аккаунта
			<br/><br/><span id="resend-link" class="waves-effect waves-light btn blue">Выслать письмо еще раз?</span>'))
				->with('_email', $check->email);
		}

		return redirect('auth/login')->withInput(Request::except('password'))
			->with('login_error', transl('Неверный логин или пароль'));

    }

    public function getLogout() {
        if(Auth::user()) {
            Cache::forget('user-is-online-' . Auth::user()->id);
            Auth::logout();
        }
        return redirect('/');
    }

    public function getReg() {
        if(Auth::user()) {
            Auth::logout();
        }
        $this->set('recaptcha', ReCaptcha::render());
    }

    public function postReg(StoreUserRequest $request) {
        if(Auth::user()) {
            Auth::logout();
        }
        $value = Request::input('g-recaptcha-response');
        $gResponse = ReCaptcha::parseInput($value);

        if ($gResponse->isSuccess()) {

            $email = Input::get('email', '');
            $firstname = Input::get('firstname', '');
            $lastname = Input::get('lastname', '');
            $code = md5(uniqid('', true));

            $user = new User;
            $user->email = $email;
            $user->firstname = $firstname;
            $user->lastname = $lastname;
            $user->password = Hash::make(Input::get('password', null));
            $user->is_active = 0;
            $user->activate_code = $code;
            $user->password_code = '';
            $user->save();
            Mail::send('auth/regmail', ['code' => $code], function ($message) use ($email) {
                $message->from(env('SERVICE_MAIL'));
                $message->to($email)->subject('Регистрация прошла успешно');
            });
            return redirect('auth/login')->with('login_notice', transl("Регистрация прошла успешно<br/><br/>На вашу почту было выслано письмо c инструкцией для активации аккаунта"));
        }
        else {
            return redirect('auth/reg')->with('error', transl('Подтвердите, что вы не робот'));
        }
    }

    public function getPasswordChangeRequest() {
        $this->set('recaptcha', ReCaptcha::render());
    }

    public function postPasswordChangeRequest() {
        if(Auth::user()) {
            Auth::logout();
        }
        $value = Request::input('g-recaptcha-response');
        $gResponse = ReCaptcha::parseInput($value);

        if ($gResponse->isSuccess()) {
            $email = Input::get('email', '');
            $user = User::where('email', '=', $email)->first();
            if ($user == null) return redirect('auth/password-change-request')->with('error', transl('Пользователя с такой почтой не существует'));

            $code = md5(uniqid('', true));
            $email = $user->email;

            $user->password_code = $code;
            $user->save();

            Mail::send('auth/passmail', ['code' => $code], function ($message) use ($email) {
                $message->from(env('SERVICE_MAIL'));
                $message->to($email)->subject('Восстановление пароля');
            });
            return redirect('auth/login')->with('login_notice', transl("На вашу почту было выслано письмо c инструкцией для восстановления пароля"));
        }
        else {
            return redirect('auth/password-change-request')->with('error', transl('Подтвердите, что вы не робот'));
        }
    }

    public function getPasswordChange($code = '') {
        if(Auth::user()) {
            Auth::logout();
        }
        if ($code != '')
        {
            $user = User::where('password_code', '=', $code)->first();
            if ($user == null )
            {
                return redirect('auth/login')->with('error', transl("Неверная ссылка для смены пароля"));
            }
            else {$this->set('code', $code);}
        }
        else return redirect('auth/login')->with('login_error', transl("Неверная ссылка для смены пароля"));

    }

    public function postPasswordChange($code = '') {
        if(Auth::user()) {
            Auth::logout();
        }
        $password = Input::get('password', '');
        $password_confirmation = Input::get('password_confirmation', '');
        $user = User::where('password_code', '=', $code)->first();
        if ($user == null )
        {
            return redirect('auth/login')->with('error', transl("Неверная ссылка для смены пароля"));
        }
        else
        {
            if ($password != $password_confirmation)
                return redirect('auth/password-change/'.$code)->with('error', transl("Введенные пароли не совпадают"));
            else
            {
                $user->password = Hash::make($password);
                $user->password_code = '';
                $user->save();
                return redirect('auth/login')->with('login_notice', transl("Пароль успешно изменен"));
            }
        }

    }

    public function getActivate($code = '') {
        if(Auth::user()) {
            Auth::logout();
        }
        $user = User::where('activate_code', '=', $code)->first();
        if ($user != null && $code != '')
        {
            $user->is_active = true;
            $user->activate_code = '';
            $user->save();
            return redirect('auth/login')->with('login_notice', transl("Активация аккаунта прошла успешно<br/><br/>Введите ваши почту и пароль, чтобы войти"));
        }
        else
        {
            return redirect('auth/login')->with('login_error', transl("Аккаунт, который вы пытаетесь активировать, не существует"));
        }
    }

}
