<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Request, App, Validator, DB, Hash, Cookie, Auth;

use Illuminate\Support\Facades\Redirect;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use App\Models\User;
use Illuminate\Support\Facades\Cache;

class UsersController extends Controller
{

    public function before()
    {
        parent::before();
        $this->section = 'admin-panel';
        $this->set('_section', 'admin-panel');
        $this->set('_section_mobile', 'users');        
    }


    public function getIndex($role=0)
    {
        if (!$this->user->is_admin)
        {
            return redirect('/');
        }
        if($role == 0){
            $users = User::orderBy('id');
        }

        elseif($role == 1){
            $users = User::where('role', '=', 1);
        }
        else{
            $users = User::where('role', '=', 2);
        }

        $total = $users->count();

        $all = $users->get();
        $online = 0;
        foreach ($all as $item)
        {
            if (Cache::has('user-is-online-' . $item->id)) $online++;
        }

        $users = $users->paginate(50);
        $users->appends(Request::all());

        $this->set('users', $users)->set('total', $total)->set('online', $online);
    }

/*
    public function getDelete($id)
    {
        if (!$this->user->is_admin)
        {
            return redirect('/');
        }

        $user = User::find((int)$id);
        if (!$user || $user->is_admin)
        {
            App::abort(404);
        }

        $this->set('user', $user);

    }

    public function postDelete($id)
    {
        if (!$this->user->is_admin)
        {
            return redirect('/');
        }

        $user = User::find((int)$id);
        if (!$user || $user->is_admin || $user->is_active)
        {
            App::abort(404);
        }

        $user->delete();

        return redirect('users' )->with('notice', 'Пользователь удален');
    }
*/

    public function getAdmin($id)
    {
        if ($this->user->is_admin != 1)
        {
            return redirect('/');
        }

        $user = User::find((int)$id);
        if (!$user || Auth::user()->is_admin != 1)
        {
            App::abort(404);
        }

        $this->set('user', $user);

    }

    public function postAdmin($id)
    {
        if ($this->user->id != 1)
        {
            return redirect('/');
        }

        $user = User::find((int)$id);
        if (!$user || Auth::user()->id != 1)
        {
            App::abort(404);
        }

        $user->is_admin = !$user->is_admin;
        $user->save();

        if ($user->is_admin) $message = 'Пользователь успешно назначен администратором';
        else $message = 'Пользователь больше не обладает правами администратора';

        return redirect('users' )->with('notice', $message);
    }


        public function postTeacher($id)
    {
        if ($this->user->id != 1)
        {
            return redirect('/');
        }

        $user = User::find((int)$id);
        if (!$user || Auth::user()->id != 1)
        {
            App::abort(404);
        }

        if($user->role != 2){
         $user->role = 2;
       
        }
        else{
            $user->role = 1;
        }
         $user->save();
        if ($user->role == 2) $message = 'Пользователь успешно назначен преподавателем';
        else $message = 'Пользователь успешно назначен студентом';

        return redirect('users' )->with('notice', $message);
    }

    public function getEditTeacher($id)
    {
        $user = User::find($id);
        $this->set('user', $user);
    }

    public function postEditTeacher($id)
    {
        $user = User::find($id);
        $user->firstname = Input::get('firstname', '') == '' ? $user->firstname :  Input::get('firstname', '');
        $user->lastname = Input::get('lastname', '') == '' ? $user->lastname :  Input::get('lastname', '');
        $user->fathers_name = Input::get('fathers_name', '') == '' ? $user->fathers_name :  Input::get('fathers_name', '');
        $user->description = Input::get('description', '') == '' ? $user->description :  Input::get('description', '');

        if(Input::hasfile('image')){

            $item = Input::file('image');
            $extention = $item->getClientOriginalExtension();
            $user->filename = $user->id.'.'.$extention;   
            if (Input::file('image') != null && is_file("storage/avatar/" . $user->filename . "/")) {
                self::delete_files("storage/avatar/" . $user->filename . "/");            
            }
            file_put_contents('storage/avatar/'. $user->id . '.' .$extention , file_get_contents($item->getRealPath()));
            

        }

        $user->save();

        return redirect('/users');
    }


}