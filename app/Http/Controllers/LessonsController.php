<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App, Validator, DB, Hash, Cookie, Auth;
use App\Models\Course;
use App\Models\Lesson;
use App\Models\File;
use App\Models\Video;
use App\Models\LessonsVisits;
use App\Models\ChatterCategory;
use App\Models\ChatterDiscussion;
use App\Models\ChatterPost;
use App\Models\Term;

class LessonsController extends Controller
{
    public function getLessons($id = 0) {
        $lessons = Lesson::where('course_id', '=', $id)->paginate(20);
        $this->set('lessons', $lessons);

        $course = Course::find($id);
        $this->set('course', $course); 
    
    }

    public function getLesson($course_id = 0, $lesson_id = 0){

        if (Cookie::get('last-lesson-' . $course_id . '-' . Auth::user()->id) == null) {
            Cookie::queue(Cookie::make('last-lesson-' . $course_id . '-' . Auth::user()->id, $lesson_id, 2628000));
        }

        $lesson = Lesson::where('id', '=', $lesson_id)->first();
        $this->set('lesson', $lesson);

        $lessons = Lesson::where('course_id', '=', $course_id)->get();
        $this->set('lessons', $lessons);

        $forum = ChatterCategory::where('order', '=', $course_id)->first();
        $discussions = ChatterDiscussion::where('chatter_category_id', '=', $forum->id)->pluck('id')->toArray();
        $last_messages = ChatterPost::whereIn('chatter_discussion_id', $discussions)->orderBy('updated_at', 'desc')->take(4)->get();

        $this->set('forum', $forum)->set('last_messages', $last_messages);

        $terms = Term::where('course_id', '=', $course_id)->get();
        $this->set('terms', $terms);

        $this->set('course_id', $course_id);

    }

    public function getLessonContent($course_id = 0, $lesson_id = 0){

        if (Cookie::get('last-lesson-' . $course_id . '-' . Auth::user()->id) == null) {
            Cookie::queue(Cookie::make('last-lesson-' . $course_id . '-' . Auth::user()->id, $lesson_id, 2628000));
        }

        $lesson = Lesson::where('id', '=', $lesson_id)->first();
        $this->set('lesson', $lesson);

        $forum = ChatterCategory::where('order', '=', $course_id)->first();
        $discussions = ChatterDiscussion::where('chatter_category_id', '=', $forum->id)->pluck('id')->toArray();
        $last_messages = ChatterPost::whereIn('chatter_discussion_id', $discussions)->orderBy('updated_at', 'desc')->take(4)->get();

        $this->set('forum', $forum)->set('last_messages', $last_messages);

    }


    public function getEdit($course_id = 0, $lesson_id = 0) {
        $course = Course::find($course_id);
        $this->set('course', $course);

        $lesson = Lesson::find($lesson_id);   
        $this->set('lesson', $lesson);

        $files = File::where('lesson_id', '=', $lesson_id)->get();
        $this->set('files', $files);

        $videos = Video::where('lesson_id', '=', $lesson_id)->get();
        $this->set('videos', $videos);
    }

    public function postEdit($course_id = 0, $lesson_id = 0) {

        $lesson_name = Input::get('lesson_name');
        $lesson_description = Input::get('lesson_description');


        if ($lesson_id == 'new') {
            $lesson = new Lesson;
            $lesson->test_id = 0;
        }
        else {
            $lesson = Lesson::find($lesson_id);

            $files = File::where('lesson_id', '=', $lesson->id)->get();
            $old_files = Input::get('old_files') == null ? [] : Input::get('old_files');

            foreach ($files as $file) {
                if (!in_array($file->id, $old_files)) {
                    self::delete_files("storage/documents/" . $file->path . "/");
                    $file->delete();
                }
            }

            $videos = Video::where('lesson_id', '=', $lesson->id)->get();
            $old_videos = Input::get('old_videos') == null ? [] : Input::get('old_videos');

            foreach ($videos as $video) {
                if (!in_array($video->id, $old_videos)) {
                    self::delete_files("storage/videos/" . $video->link);
                    $video->delete();
                }
            }   
        }

        $lesson->lesson_name = $lesson_name;
        $lesson->lesson_description = $lesson_description;   
        $lesson->course_id = $course_id;
        $lesson->save();

        if (Input::file('files') != null) {
            foreach (Input::file('files') as $item) {

                $filename = str_replace(' ', '_', $item->getClientOriginalName());   

                $path = substr(md5(uniqid('', true)), 0, 32);

                mkdir("storage/documents/" . $path);
                file_put_contents('storage/documents/'. $path . '/' . $filename, file_get_contents($item->getRealPath()));
            
                $file = new File;
                $file->lesson_id = $lesson->id;
                $file->filename = $filename;
                $file->path = $path;
                $file->save();
            }
        }

        if (Input::file('videos-new') != null) {
            foreach (Input::file('videos-new') as $key => $item) {
                if ($item != null) {


                    $filename = substr(md5(uniqid('', true)), 0, 32) . '.' . $item->getClientOriginalExtension();

                    file_put_contents('storage/videos/' . $filename, file_get_contents($item->getRealPath()));

                    $video = new Video;
                    $video->lesson_id = $lesson->id;
                    $video->link = $filename;
                    $video->name = Input::get('video-new-name')[$key];
                    $video->save();                    
                }
            }
        }        

        if (Input::get('old_videos') != null) {
            foreach (Input::get('old_videos') as $key => $item) {
                if ($item != null) {
                    $video = Video::where('id', '=', $item)->first();
                    $video->name = Input::get('video-name')[$key];
                    $video->save();                    
                }
            }
        }

        if (Input::file('videos-edit') != null) {
            foreach (Input::file('videos-edit') as $key => $item) {
                if ($item != null) {
                    $video = Video::where('id', '=', Input::get('old_videos')[$key])->first();

                    self::delete_files("storage/videos/" . $video->link);

                    $filename = substr(md5(uniqid('', true)), 0, 32) . '.' . $item->getClientOriginalExtension();

                    file_put_contents('storage/videos/' . $filename, file_get_contents($item->getRealPath()));

                    $video->link = $filename;
                    $video->save();
                }
            }
        }



        $result['status'] = 'ok';
        $result['lesson_id'] = $lesson->id;

        return \Response::json($result);
	}

    public function postDelete() {
        $lesson_id = Input::get('lesson_id');

        $lesson = Lesson::find($lesson_id);

        if ($lesson == null) return "Lesson not found";

        $files = File::where('lesson_id', '=', $lesson->id)->get();

        foreach ($files as $file) {
            self::delete_files("storage/documents/" . $file->path . "/");
            $file->delete();
        }

        $videos = Video::where('lesson_id', '=', $lesson->id)->get();

        foreach ($videos as $video) {

            self::delete_files("storage/videos/" . $video->link);

            $video->delete();
        }

        $lesson->delete();

        $result['status'] = 'ok';

        return \Response::json($result);
    }

    public function postVisit() {

        if (Auth::user()->id != Input::get('user', 0)) {
            return "wrong user";
        }

        $lesson_id = Input::get('lesson');

        $lesson = Lesson::find($lesson_id);

        if ($lesson == null) return "Lesson not found";

        $visit = LessonsVisits::where('user_id','=', Auth::user()->id)->where('lesson_id', '=', $lesson->id)->first();
        if ($visit == null) {
            $visit = new LessonsVisits;
            $visit->user_id = Auth::user()->id;
            $visit->lesson_id = $lesson->id;
            $visit->save();
        }

        $result['status'] = 'ok';
        $result['progress'] = $lesson->course()->progress();

        return \Response::json($result);
    }


    static function delete_files($target) {
        if (is_dir($target)) {

            $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

            foreach( $files as $file )
            {
                self::delete_files( $file );
            }

            rmdir( $target );

        } elseif (is_file($target)) {
            unlink( $target );
        }
    }


}
