<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Request, App, Validator, DB, Hash, Cookie, Auth;
use Maatwebsite\Excel\Facades\Excel;

use App\Models\User;
use App\Models\Course;
use App\Models\Lesson;
use App\Models\Category;
use App\Models\MyCourses;
use App\Models\ChatterCategory;
use App\Models\Term;


class CoursesController extends Controller
{
    public function before()
    {
        parent::before();
        $this->section = 'courses';
        $this->set('_section', 'courses');
    }


    public function getIndex($user_id = 0)
    {
        $this->set('_section', 'admin-panel');

        if (!Auth::user()){
            return redirect('/');
        }

        if ($user_id == 0 ) $user_id = Auth::user()->id;

        $user = User::where('id', '=', $user_id)->first();
    }


    public function getCreateCourse()
    {
        $users = User::where('role', '=', 2)->get();
        $this->set('users', $users);
        $categories = Category::all();
        $this->set('categories', $categories);
    }

    public function getCategories()
    {

    }

    public function postCreateCourse()
    {
        $course_name = Input::get('course_name');
        $description = Input::get('description');
        $category_id = Input::get('category');
        $course_price = Input::get('course_price');
        $teacher_id = Input::get('teacher_id');

        $item = Input::file('image');
        $extention = $item->getClientOriginalExtension();


        if ($course_name == null) {
            return 'err';
        }

        if ($description == null) {
            return 'err';
        }

        $info = new Course;
        $info->course_name = $course_name;
        $info->user_id = $teacher_id;
        $info->description = $description;
        $info->category_id = $category_id;
        $info->course_price = $course_price;
        $info->user_id = $teacher_id;
        $info->filename = 'dfr';
        $info->start_at = '1970-01-01';
        $info->finish_at = '1970-01-01';
        $info->save();
        $info->filename = $info->id.'.'.$extention;
        $info->save();

        $chatter_category = new ChatterCategory;
        $chatter_category->parent_id = null;
        $chatter_category->order = $info->id;
        $chatter_category->name = $course_name;
        $chatter_category->color = '#' . substr(dechex(crc32($course_name)), 0, 6);
        $chatter_category->slug = $info->id;
        $chatter_category->save();

        file_put_contents('storage/wallpapers/'. $info->id . '.' .$extention , file_get_contents($item->getRealPath()));

        return redirect('/courses/allcourses');
    }

    public function getCourse($id){
        $course = Course::find($id);
        $this->set('course', $course);

    }

    public function getAllcourses()
    {
        $this->set('_section', 'admin-panel');

        $courses = Course::all();
        $this->set('courses', $courses);

        $users = User::where('role', '=', 2)->get();
        $this->set('users', $users);

    }


    public function getEdit($id)
    {
        $this->set('_section', 'admin-panel');
        $course = Course::find($id);
        $this->set('course', $course);
        $users = User::where('role', '=', 2)->get();
        $this->set('users', $users);
        $lesson = Lesson::where('course_id', '=', $id)->get();
        $this->set('lesson', $lesson);
        $categories = Category::all();
        $this->set('categories', $categories);

    }
    public function postUpdate($id)
    {
        $courses = Course::find($id);
        $courses->course_name = Input::get('course_name', '') == '' ? $courses->course_name :  Input::get('course_name', '');
        $courses->user_id =Input::get('teacher_id') == '' ? $courses->user_id : Input::get('teacher_id', '');
        $courses->description = Input::get('description') == '' ? $courses->description :  Input::get('description', '');
        $courses->category_id = Input::get('category_id') == '' ? $courses->category_id :  Input::get('category_id', '');
        $courses->course_price = Input::get('course_price') == '' ? $courses->course_price :  Input::get('course_price', '');
        $courses->test_id = 0;

        if(Input::hasfile('image')){

            $item = Input::file('image');
            $extention = $item->getClientOriginalExtension();
            $courses->filename = $courses->id.'.'.$extention;

            if (Input::file('image') != null && is_file("storage/wallpaper/" . $courses->filename . "/")) {
                self::delete_files("storage/wallpaper/" . $courses->filename . "/");
            }
            file_put_contents('storage/wallpapers/'. $courses->id . '.' .$extention , file_get_contents($item->getRealPath()));
        }

        $courses->save();

        if(Input::hasfile('terms')){
            $file = Input::file('terms');
            $course_id = $courses->id;
            $term_id = 0;

            Excel::setDelimiter('|');
            Excel::load($file, function ($reader) use (&$course_id) {
                $reader->noHeading();
                $i = 0;

                foreach ($reader->toArray() as $row) {
                    $term = new Term;
                    $term->term = $row[0] . "";
                    $term->course_id = $course_id;
                    $term->definition = $row[1] ."";
                    $term->save();
                }

            });

            $result['status'] = 'ok';
            $result['course_id'] = $courses->id;

            return \Response::json($result);

        }

        $result['status'] = 'ok';
        $result['course_id'] = $courses->id;

        return \Response::json($result);


    }



    public function postDelete($id)
    {

        $lesson = Lesson::where('course_id', '=', $id);
        $lesson->delete();
        $courses = Course::find($id);
        $courses->delete();
        return redirect('courses/allcourses');
    }

    public function getMyCourses(){

        $courses = null;

        $mycourses = MyCourses::where('user_id', '=', Auth::user()->id)->pluck('course_id')->toArray();
        $this->set('mycourses', $mycourses);

        if (Auth::user()->role == 1){
            $courses = Course::whereIn('id', $mycourses)->get();
        }
        if (Auth::user()->role == 2){
            $courses = Course::whereIn('id', $mycourses)->orWhere('user_id', '=', Auth::user()->id)->get();
        }
        if (Auth::user()->role == 3){
            $courses = Course::all();
        }
        $this->set('courses', $courses);

    }



    public function postMyCourses(){
        $course_id = Input::get('course_id');
        $mycourse = new MyCourses;
        $mycourse->course_id = $course_id;
        $mycourse->user_id = Auth::user()->id;
        $mycourse->save();

        return redirect('courses/mycourses');
    }

    public function getMyStudents($id=0){

        $this->set('_section', 'admin-panel');
        
        if (Auth::user()->role != 2){
            return redirect('courses/mycourses');
        }

        $courses1 = Course::where('user_id', '=', Auth::user()->id)->get();
        $this->set('courses1', $courses1);

        $courses = Course::where('user_id', '=', Auth::user()->id);
        if ($id > 0 ) $courses = $courses->where('id', '=', $id);         
        $courses = $courses->get();
        $this->set('courses', $courses); 
        
        $students = [];
        foreach ($courses as $course) {
            $my_students = MyCourses::where('course_id', '=', $course->id)->pluck('user_id')->toArray();
            $students[$course->id] = User::whereIn('id', $my_students)->where('role', '=', 1)->get();
        }

        $this->set('students', $students);

      }



    public function getStudents(){

        $this->set('_section', 'admin-panel');

        $mycourses = MyCourses::where('course_id', '=', $id)->pluck('user_id')->toArray();
        $this->set('mycourses', $mycourses);

        $users = User::whereIn('id', $mycourses)->get();
        $this->set('users', $users);

        $course = Course::where('id', '=', $id)->first();
        $this->set('course', $course);

    }

}
