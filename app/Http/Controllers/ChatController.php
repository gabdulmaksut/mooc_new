<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use App\Models\Chat;
use App\Models\User;
use App\Models\DialogueParticipant;
use App\Models\SocketIO;

class ChatController extends Controller
{

    public function before()
    {
        parent::before();
        $this->section = 'chat';
        $this->set('_section', 'chat');
    }



	public function getIndex() {

		$dp = DialogueParticipant::where('dialogue_id', '=', Auth::user()->id)->pluck('user_id')->toArray();

		$users = User::whereIn('users.id', $dp)->get();

    	$interlocutor = isset($_COOKIE['interlocutor']) ? $_COOKIE['interlocutor'] : null;
		$interlocutor_info = null;
		$chat = [];

        $embed = Input::get('embed', null);
        if ($embed != null) {
            $interlocutor = Input::get('interlocutor', 0);
        }

		if ($interlocutor != null) {

			$chat = Chat::where('from', '=', $interlocutor)->where('to', '=', Auth::user()->id)->where('is_read', '=', 0)->update(['is_read' => 1]);

			$chat = Chat::where(function ($query) use($interlocutor)  {
    			$query->where('from', '=', Auth::user()->id)
        			->where('to', '=', $interlocutor);
        	})->orWhere(function($query) use($interlocutor) {
			    $query->where('from', '=', $interlocutor)
			        ->where('to', '=', Auth::user()->id);	
			})->orderBy('created_at', 'desc')->take(36)->get(); 

			$interlocutor_info = User::where('id', '=', $interlocutor)->first();

		}

		$key = 0;
		while (isset($chat[$key])) {
			$key++;
		}

		$this->set('users', $users)
            ->set('interlocutor', $interlocutor)
            ->set('interlocutor_info', $interlocutor_info)
            ->set('chat', $chat)
            ->set('oldest_message', isset($chat[$key - 1]) ? $chat[$key - 1] : null)
            ->set('embed', $embed);

    }



	public function postIndex() {
		$interlocutor = Input::get('interlocutor', 0);

		if ($interlocutor > 0) {
			$dialogue_id = Auth::user()->id;

			$dp = DialogueParticipant::where('dialogue_id', '=', Auth::user()->id)
        			->where('user_id', '=', $interlocutor)->first();

        	if ($dp == null) {
        		$dp = new DialogueParticipant;
        		$dp->dialogue_id = Auth::user()->id;
        		$dp->user_id = $interlocutor;
        		$dp->save();
        	}

        	setcookie("interlocutor", $interlocutor, intval(time() + 60*60*24*30));
		}

		return 'ok';
    }



	public function postSendMessage() {

		$message = new Chat();
		$message->from = Auth::user()->id;
		$message->to = (int)Input::get('interlocutor', 0);
		$message->message = (string)Input::get('message', '');
		$message->is_read = Auth::user()->id == (int)Input::get('interlocutor', 0) ? true : false;
		$message->code = (string)Input::get('message_id', '');
		$message->save();


		$dp = DialogueParticipant::where('dialogue_id', '=', (int)Input::get('interlocutor', 0))
    			->where('user_id', '=', Auth::user()->id)->first();

    	if ($dp == null) {
    		$dp = new DialogueParticipant;
    		$dp->dialogue_id = (int)Input::get('interlocutor', 0);
    		$dp->user_id = Auth::user()->id;
    		$dp->save();
    	}


		$result['status'] = 'ok';
		$result['created_at'] = $message->created_at;
		$result['message_id'] = $message->code;

		$socketio = new SocketIO();
		$socketio->send('ssl://devtest.psu.kz', 8443, '_message', '{"from": "' . $message->from  . '", "room": "' . md5($message->to)  . '", "message": ' . json_encode($message->message, JSON_HEX_APOS) . ', "created_at": "' . $message->created_at . '", "code": "' . $message->code . '", "sender_name": ' .  json_encode(((Auth::user()->is_admin ? "<span class='admin-label'>admin</span> " : "") . strip_tags(Auth::user()->firstname) . ' ' .  strip_tags(Auth::user()->lastname)), JSON_HEX_APOS) . '}');

		return \Response::json($result);
	}

	public function postTakeOld() {

		$timestamp = Input::get('timestamp', 0);
		$interlocutor = (int)Input::get('interlocutor', 0);
		$oldest_message_code = (string)Input::get('oldest_message', '');
		$oldest_message = Chat::where('code', '=', $oldest_message_code)->first();
		$chat = null;

		if (isset($oldest_message->id)) {
		
			$oldest_message_id = $oldest_message->id;

			if ($interlocutor != 0) {
				$chat = Chat::where(function ($query) use($interlocutor, $oldest_message_id)  {
					$query->where('id', '<', $oldest_message_id);
				})->where(function ($query) use($interlocutor)  {
					$query->where(function ($query) use($interlocutor)  {
		    			$query->where('from', '=', Auth::user()->id)
		        			->where('to', '=', $interlocutor);
		        	})->orWhere(function($query) use($interlocutor) {
					    $query->where('from', '=', $interlocutor)
					        ->where('to', '=', Auth::user()->id);	
					});
	        	})->orderBy('created_at', 'desc')->take(18)->get(); 
			}		

		}
		else {

			if ($interlocutor != 0) {

				$chat = Chat::where('from', '=', $interlocutor)->where('to', '=', Auth::user()->id)->where('is_read', '=', 0)->update(['is_read' => 1]);

				$chat = Chat::where(function ($query) use($interlocutor)  {
	    			$query->where('from', '=', Auth::user()->id)
	        			->where('to', '=', $interlocutor);
	        	})->orWhere(function($query) use($interlocutor) {
				    $query->where('from', '=', $interlocutor)
				        ->where('to', '=', Auth::user()->id);	
				})->orderBy('created_at', 'desc')->take(36)->get(); 
			}

		}


		$result['status'] = 'ok';
		$result['chat'] = $chat;
		$result['timestamp'] = $timestamp;
		$result['unread'] = Auth::user()->unreadMessagesCount();

		return \Response::json($result);
	}	

	public function postSetRead() {

		$interlocutor = (int)Input::get('interlocutor', 0);

		if ($interlocutor != 0) {

			$chat = Chat::where('from', '=', $interlocutor)->where('to', '=', Auth::user()->id)->where('is_read', '=', 0)->update(['is_read' => 1]);

		}

		return 'ok';
	}	

}
