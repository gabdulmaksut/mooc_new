<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Request, App, Validator, DB, Hash, Cookie, Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Redirect;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

use App\Models\Test;
use App\Models\Question;
use App\Models\QuestionOption;
use App\Models\Image;
use App\Models\Attempt;
use App\Models\User;
use App\Models\Course;
use App\Models\Lesson;
use App\Models\Category;
use App\Models\MyCourses;

class CategoriesController extends Controller
{
    public function before()
    {
        parent::before();
        $this->section = 'categories';
        $this->set('_section', 'categories');
    }

    public function getIndex($id=0)
    {    
        $this->set('this_category', $id);    
        $categories = Category::orderBy('id')->paginate();
        $this->set('categories', $categories);
       
        $term  = Input::get('search', null);
        if ($term != null) {

            $term = Input::get('search');
            $courses = Course::where('course_name', 'LIKE', '%' . $term . '%')->paginate();
        }

        elseif ($id ==0){
        $courses = Course::all();
        } 
         
        else {  
        $courses = Course::where('category_id','=', $id)->paginate(9);           
        }

       $this->set('courses', $courses);
    }

    

      public function postIndex($id)
    {
        $categories = Category::all();
        $this->set('categories', $categories);
    }

    public function getCourse($id){
        $course = Course::find($id);
        $this->set('course', $course);

        $mycourses = null;
        $mine = 0;

        if (Auth::user()) {
            $mycourses = MyCourses::where('user_id', '=', Auth::user()->id)->pluck('course_id')->toArray();

            if (in_array($id, $mycourses)) {
                $mine = 1;
            }
        }

        $this->set('mycourses', $mycourses)->set('mine', $mine);

        $lesson = $course->last_lesson();
        $this->set('lesson', $lesson);


    }

    public function postCourse($id){

     }

     public function getCreateCategory()
    {
        $this->set('_section', 'admin-panel');
        $categories = Category::all();
        $this->set('categories', $categories);
    }
       
        public function postCreateCategory()
    {
        $category_name = Input::get('category_name');
        
        $item = Input::file('ava');
        $extention = $item->getClientOriginalExtension();

        $category = new Category;
        $category->category_name = $category_name;
        $category->filename = 'dfr';
        $category->save(); 
        $category->filename = $category->category_name.'.'.$extention;
        $category->save();

        file_put_contents('storage/wallpapers/'. $category->category_name . '.' .$extention , file_get_contents($item->getRealPath()));

        return redirect('/courses');
    }

     public function getAllCategories()
     {
        $this->set('_section', 'admin-panel');
        $categories = Category::all();
        $this->set('categories', $categories);
     }

     public function postAllCategories(){

     }

     public function getEdit($id){
        $this->set('_section', 'admin-panel');
        $category = Category::find($id);
        $this->set('category', $category);

     }

     public function postEdit($id){

        $category = Category::find($id);
        $category->category_name = Input::get('category_name', '') == '' ? $category->category_name :  Input::get('category_name', '');
        $category->save();

        $item = Input::file('image');
        $extention = $item->getClientOriginalExtension();
        $category->filename = $category->category_name.'.'.$extention; 
        
        if (Input::file('image') != null && is_file("storage/wallpaper/" . $category->filename . "/")) {
            self::delete_files("storage/wallpaper/" . $category->filename . "/");            
        }
        file_put_contents('storage/wallpapers/'. $category->category_name . '.' .$extention , file_get_contents($item->getRealPath()));

        $category->save();

        return redirect('/categories/allcategories');

     }

    
}