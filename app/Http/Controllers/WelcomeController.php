<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\Test;
use App\Models\Question;
use App\Models\QuestionOption;
use Illuminate\Support\Facades\Cookie;
use App\Models\Course;
use App\Models\Category;

class WelcomeController extends Controller
{

    public function before()
    {
        parent::before();
        $this->section = 'welcome';
        $this->set('_section', 'welcome');
    }

    public function getIndex()
    {
    	$courses = Course::orderBy('id', 'desc')->take(4)->get();
        $this->set('courses', $courses);
        $categories = Category::orderBy('id', 'desc')->take(3)->get();
        $this->set('categories', $categories);
        $count=0;
    }



}