<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Request, App, Validator, DB, Hash, Cookie, Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Redirect;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

use App\Models\Test;
use App\Models\Question;
use App\Models\QuestionOption;
use App\Models\Image;
use App\Models\Attempt;
use App\Models\User;
use App\Models\Course;
use App\Models\Lesson;
use App\Models\MyCourses;

class ProfileController extends Controller
{

    public function before()
    {
        parent::before();
        $this->section = 'profile';
        $this->set('_section', 'profile');
    }


    public function getIndex($user_id = 0)
    {
        if (!Auth::user()){
            return redirect('/');
        }

        if ($user_id == 0 ) $user_id = Auth::user()->id;

        $user = User::where('id', '=', $user_id)->first();

        $attempts = Attempt::join('tests', 'tests.id', '=', 'attempts.test_id')
            ->where('user_id', '=', $user_id)
            ->where('is_completed', '=', true)
            ->select(['attempts.*', 'tests.title', 'tests.questions'])
            ->orderBy('attempts.created_at')
            ->get()
            ->groupBy('test_id');

        $best = [];
        foreach ($attempts as $item) {
            foreach ($item as $attempt) {
                if (!isset($best[$attempt->test_id])) $best[$attempt->test_id] = $attempt->correct_amount;
                else {
                    if ($best[$attempt->test_id] < $attempt->correct_amount) $best[$attempt->test_id] = $attempt->correct_amount;
                }
            }
        }

        $this->set('attempts', $attempts)->set('best', $best)->set('user', $user);

        $courses = null;

        $mycourses = MyCourses::where('user_id', '=', $user->id)->pluck('course_id')->toArray();
        $this->set('mycourses', $mycourses);  

        $courses = Course::whereIn('id', $mycourses)->get();
        $this->set('courses', $courses);

    }


}