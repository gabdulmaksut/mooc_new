<?php
namespace App\Http\Controllers;
use App\Models\Image;
use App\Models\Question;
use App\Models\QuestionOption;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use DB;
use App\Models\Test;
use App\Models\Answer;
use App\Models\Attempt;
use App\Models\Lesson;
use App\Models\Course;
use App\Models\Term;
use Illuminate\Support\Facades\Cookie;

class AjaxController extends Controller
{
    public function after()
    {
    }

    public function postResendMail()
    {
        $email = Input::get('email', '');
        $code = md5(uniqid('', true));

        $user = User::where('email', '=', $email)->first();;
        $user->activate_code = $code;
        $user->save();
        Mail::send('auth/regmail', ['code' => $code], function ($message) use ($email) {
            $message->from(env('SERVICE_MAIL'));
            $message->to($email)->subject('Регистрация прошла успешно');
        });

        return 'ok';
    }

    public function postExpand()
    {
        $link= (string)Input::get('link', 0);

        if ($link)
        {
            $type = (string)Input::get('type', 0);

            if ($type == 'image')
            {
                $html = "<img id='content' src='" . $link . "'  alt=''  height='100%' width='100%' />";
            }
            else
            {
                $html = '
                    <video id="content" width="100%" controls controlsList="nodownload" oncontextmenu="return false;" preload="metadata" autoplay data-setup=\'{"fluid": true}\' >
                        <source src="' . $link . '" type="video/mp4"/>
                        Ваш браузер не поддерживает отображение видео.
                    </video>
                ';
            }

        }
        else $html = "Файл не найден";
        return $html;
    }

    public function postStartAttempt()
    {
        $user_id = Input::get('user_id', '');
        $test_id = Input::get('test_id', 0);

        $test = Test::where('id', '=', $test_id)->first();
        if ($test == null) {
            return 'Test not found';
        }

        if ($user_id == '') {
            $attempts_count = Attempt::where('cookie', '=', Cookie::get('user_id'))
                ->where('test_id', '=', $test->id)
                ->where('is_completed', '=', true)
                ->count();

        }
        else {
            $attempts_count = Attempt::where('user_id', '=', $user_id)
                ->where('test_id', '=', $test->id)
                ->where('is_completed', '=', true)
                ->count();
        }

        if ($test->attempts > 0 && $test->attempts <= $attempts_count ) {
            return 'Too many attempts';
        }

        if ($user_id == '') $user_id = Cookie::get('user_id');

        $range = range(1, $test->questions);
        shuffle($range);

        $attempt = new Attempt;
        $attempt->user_id = $user_id;
        $attempt->cookie = Cookie::get('user_id');
        $attempt->test_id = $test_id;
        $attempt->is_completed = false;
        $attempt->position = 1;
        $attempt->pattern = implode(";", $range);
        $attempt->completed_at = NULL;
        $attempt->correct_amount = 0;
        $attempt->save();

        $questions = Question::where('test_id', '=', $test->id)->get();

        $i = 0;
        foreach ($questions as $question) {

            $i++;
            $range = range(1, $question->options_count());
            shuffle($range);

            $answer = new Answer;
            $answer->number = $i;
            $answer->question_id = $question->id;
            $answer->attempt_id = $attempt->id;
            $answer->answer = '';
            $answer->pattern = implode(";", $range);
            $answer->save();
        }

        return 'ok';
    }

    public function postEndAttempt()
    {
        $user_id = Input::get('user_id', '');
        $test_id = Input::get('test_id', 0);
        $attempt_id = Input::get('attempt_id', 0);

        $test = Test::where('id', '=', $test_id)->first();
        if ($test == null) {
            return 'Test not found';
        }

        if ($user_id == '') {
            $attempt = Attempt::where('id', '=', $attempt_id)
                ->where('cookie', '=', Cookie::get('user_id'))
                ->first();
        }
        else {
            $attempt = Attempt::where('id', '=', $attempt_id)
                ->where('user_id', '=', $user_id)
                ->first();
        }

        if ($attempt == null) {
            return 'Attempt not found';
        }


        $attempt->calculate_correct();

        $attempt->is_completed = true;
        $attempt->save();

        return 'ok';
    }


    public function postSaveAnswer()
    {
        $user_id = Input::get('user_id', '');
        $attempt_id = Input::get('attempt_id', 0);
        $question_id = Input::get('question_id', 0);
        $answer_value = Input::get('answer', "");
        $position = Input::get('position', 0);

        if ($user_id == '') {
            $attempt = Attempt::where('id', '=', $attempt_id)
                ->where('cookie', '=', Cookie::get('user_id'))
                ->first();
        }
        else {
            $attempt = Attempt::where('id', '=', $attempt_id)
                ->where('user_id', '=', $user_id)
                ->first();
        }

        if ($attempt == null) {
            return 'Attempt not found';
        }

        $test = Test::where('id', '=', $attempt->test_id)->first();
        if ($test == null) {
            return 'Test not found';
        }

        if (!$attempt->is_completed) {
            $answer = Answer::where('attempt_id', '=', $attempt_id)
                ->where('question_id', '=', $question_id)
                ->first();

            $answer->answer = ($answer_value == "undefined" || $answer_value == "") ? "" : $answer_value;
            $answer->save();
        }

        $attempt->position = $position;
        $attempt->save();

        return 'ok';
    }

    public function postDeleteTest()
    {
        $user_id = Input::get('user_id', 0);
        $test_id = Input::get('test_id', 0);

        $user = User::where('id', '=', $user_id)->first();

        if ($user->role < 3 || Auth::user()->id != $user->id) {
            return 'Wrong user';
        }

        $test = Test::where('id', '=', $test_id)->first();
        if ($test == null) {
            return 'Test not found';
        }

        $questions = Question::where('test_id', '=', $test_id)->get();

        foreach ($questions as $question) {
            $question_options = QuestionOption::where('question_id', '=', $question->id)->get();
            foreach ($question_options as $question_option) {
                $question_option->delete();
            }

            $answers = Answer::where('question_id', '=', $question->id)->get();
            foreach ($answers as $answer) {
                $answer->delete();
            }

            $question->delete();
        }

        $attempts = Attempt::where('test_id', '=', $test_id)->get();
        foreach ($attempts as $attempt) {
            $attempt->delete();
        }

        $images = Image::where('test_id', '=', $test_id)->get();
        foreach ($images as $image) {
            $image->delete();
        }

        self::delete_files("assets/img/" . $test_id . "/");

        $test->delete();

        if (Input::get('lesson_id', 0) > 0) {
            $lesson = Lesson::where('id', '=', Input::get('lesson_id', 0))->first();
            $lesson->test_id = 0;
            $lesson->save();
        }

        if (Input::get('course_id', 0) > 0) {
            $course = Course::where('id', '=', Input::get('course_id', 0))->first();
            $lesson = Lesson::where('test_id', '=', $course->test_id)->delete();
            $course->test_id = 0;
            $course->save();
        }

        return 'ok';
    }

    public function postDeleteTerm()
    {
        $user_id = Input::get('user_id', 0);
        $term_id = Input::get('term_id', 0);
        $course_id = Input::get('course_id', 0);

        $user = User::where('id', '=', $user_id)->first();

        if ($user->role < 3 || Auth::user()->id != $user->id) {
            return 'Wrong user';
        }

        $term = Term::where('course_id', '=', $course_id)->delete();
        if ($term == null) {
            return 'Term not found';
        }

        return 'ok';
    }


    static function delete_files($target) {
        if (is_dir($target)) {

            $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

            foreach( $files as $file )
            {
                self::delete_files( $file );
            }

            rmdir( $target );

        } elseif (is_file($target)) {
            unlink( $target );
        }
    }


}