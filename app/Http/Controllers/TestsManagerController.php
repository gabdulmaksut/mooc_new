<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Request, App, Validator, DB, Hash, Cookie, Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Redirect;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

use App\Models\Test;
use App\Models\Question;
use App\Models\QuestionOption;
use App\Models\Image;
use App\Models\Attempt;
use App\Models\User;
use App\Models\Lesson;
use App\Models\Course;


class TestsManagerController extends Controller
{

    public function before()
    {
        parent::before();
        $this->section = 'tests';
        $this->set('_section', 'admin-panel');
        $this->set('_section_mobile', 'tests');
    }


    public function getIndex()
    {
        if (!$this->user->is_admin){
            return redirect('/');
        }

        $tests = Test::orderBy('id');

        $tests = $tests->paginate(50);
        $tests->appends(Request::all());

        $attempts_count = [];
        $attempts_count_logged = [];

        $success_count = [];
        $success_count_logged = [];

        foreach ($tests as $test) {
            $attempts_count[$test->id] = Attempt::where('test_id', '=', $test->id)->distinct('user_id')->count('user_id');
            $attempts_count_logged[$test->id] = Attempt::join('users', 'users.id', '=', 'attempts.user_id')
                ->where('test_id', '=', $test->id)
                ->distinct('user_id')
                ->count('user_id');

            $success_count[$test->id] = 0;
            $success_count_logged[$test->id] = 0;

            $attempts = Attempt::where('test_id', '=', $test->id)->get();
            $users_rating = [];
            foreach ($attempts as $attempt) {
                if (!isset($users_rating[$attempt->user_id])) $users_rating[$attempt->user_id] = 0;
                if ($users_rating[$attempt->user_id] < $attempt->correct_amount) $users_rating[$attempt->user_id] = $attempt->correct_amount;
            }



            foreach ($users_rating as $key => $item) {
                if ($test->questions > 0 && $item / $test->questions >= 0.8) $success_count[$test->id]++;

                $user =  User::where('id', '=', $key)->first();

                if ($user) $success_count_logged[$test->id]++;
            }


        }

        $this->set('tests', $tests)
            ->set('attempts_count', $attempts_count)
            ->set('attempts_count_logged', $attempts_count_logged)
            ->set('success_count', $success_count)
            ->set('success_count_logged', $success_count_logged);
    }

/*
    public function getDelete($id)
    {
        if (!$this->user->is_admin)
        {
            return redirect('/');
        }

        $user = User::find((int)$id);
        if (!$user || $user->is_admin)
        {
            App::abort(404);
        }

        $this->set('user', $user);

    }

    public function postDelete($id)
    {
        if (!$this->user->is_admin)
        {
            return redirect('/');
        }

        $user = User::find((int)$id);
        if (!$user || $user->is_admin || $user->is_active)
        {
            App::abort(404);
        }

        $user->delete();

        return redirect('users' )->with('notice', 'Пользователь удален');
    }
*/

    public function getLoad()
    {
        if ($this->user->is_admin != 1){
            return redirect('/');
        }
    }

    public function postLoad()
    {
        if ($this->user->role < 3){
            return redirect('/');
        }

        $files = Input::file('tests');
        $count = count($files);

        try {
            foreach ($files as $file) {
                $test_id = 0;

                Excel::setDelimiter('|');
                Excel::load($file, function ($reader) use (&$test_id) {
                    $reader->noHeading();
                    $i = 0;
                    $test = new Test;
                    foreach ($reader->toArray() as $row) {

                        if ($i > 0 && $row[0] != null && $row[1] != null && $row[2] != null) {
                            $question = new Question;
                            $question->test_id = $test->id;
                            $question->number = $i;
                            $question->text = $row[0] . "";
                            $question->type = intval($row[1]);
                            $question->coordinates = 'A' . ($i + 1);
                            $question->save();

                            if ($row[1] == 4){
                                $correct_answers = explode(";", $row[2]);
                                $correct_relations = [];
                                foreach ($correct_answers as $item) {
                                    $correct_relations[explode("-", $item)[0]] = explode("-", $item)[1];
                                }
                                $objects_count = explode(";", $row[3])[0];
                                $cells_count = explode(";", $row[3])[1];
                                for ($j = 4; $j < $objects_count + 4; $j++) {
                                    $question_option = new QuestionOption;
                                    $question_option->question_id = $question->id;
                                    $question_option->number = $j - 3;
                                    $question_option->text = $row[$j] . "";
                                    $question_option->is_correct = true;
                                    $question_option->correct_relation = isset($correct_relations[$j - 3]) ? $correct_relations[$j - 3] : 0;
                                    $question_option->coordinates = Coordinate::stringFromColumnIndex($j + 1) . ($i + 1);
                                    $question_option->save();
                                }
                                for ($j = 4 + $objects_count; $j < $objects_count + $cells_count + 4; $j++) {
                                    $question_option = new QuestionOption;
                                    $question_option->question_id = $question->id;
                                    $question_option->number = $j - 3 - $objects_count;
                                    $question_option->text = $row[$j] . "";
                                    $question_option->is_correct = false;
                                    $question_option->correct_relation = 0;
                                    $question_option->coordinates = Coordinate::stringFromColumnIndex($j + 1) . ($i + 1);
                                    $question_option->save();
                                }
                            }
                            elseif ($row[1] < 3) {
                                $correct_answers = explode(";", $row[2]);
                                $j = 3;
                                while (isset($row[$j])) {
                                    $question_option = new QuestionOption;
                                    $question_option->question_id = $question->id;
                                    $question_option->number = $j - 2;
                                    $question_option->text = $row[$j] . "";
                                    $question_option->is_correct = in_array(($j - 2), $correct_answers);
                                    $question_option->coordinates = Coordinate::stringFromColumnIndex($j + 1) . ($i + 1);
                                    $question_option->save();
                                    $j++;
                                }
                            } else {
                                $question_option = new QuestionOption;
                                $question_option->question_id = $question->id;
                                $question_option->number = 1;
                                $question_option->text = $row[2];
                                $question_option->is_correct = true;
                                $question_option->coordinates = "";
                                $question_option->save();
                            }

                            $i++;
                        }

                        if ($i == 0) {
                            $test->title = $row[0];
                            $test->questions = 0;
                            $test->is_free = ($row[1] == 0) ? true : false;
                            $test->attempts = intval($row[2]);
                            $test->time = intval($row[3]);
                            $test->save();

                            $i++;
                        }
                    }
                    $test->questions = $i - 1;
                    $test->save();

                    $test_id = $test->id;
                });

                $objPHPExcel = IOFactory::load($file);

                $i = 0;
                foreach ($objPHPExcel->getActiveSheet()->getDrawingCollection() as $drawing) {
                    if ($drawing instanceof MemoryDrawing) {
                        ob_start();
                        call_user_func(
                            $drawing->getRenderingFunction(),
                            $drawing->getImageResource()
                        );
                        $imageContents = ob_get_contents();
                        ob_end_clean();
                        $cellID = $drawing->getCoordinates();

                        switch ($drawing->getMimeType()) {
                            case MemoryDrawing::MIMETYPE_PNG :
                                $extension = 'png';
                                break;
                            case MemoryDrawing::MIMETYPE_GIF:
                                $extension = 'gif';
                                break;
                            case MemoryDrawing::MIMETYPE_JPEG :
                                $extension = 'jpg';
                                break;
                        }
                    }

                    if ($i == 0) {
                        self::delete_files("assets/img/" . $test_id . "/");
                        mkdir("assets/img/" . $test_id);
                    }
                    $myFileName = '00_Image_' . ++$i . '.' . $extension;
                    file_put_contents("assets/img/" . $test_id . "/" . $myFileName, $imageContents);

                    $image = new Image;
                    $image->test_id = $test_id;
                    $image->coordinates = $cellID;
                    $image->path = "assets/img/" . $test_id . "/" . $myFileName;
                    $image->save();
                }

                if ($i == 0) {
                    self::delete_files("assets/img/" . $test_id . "/");
                }

            }

            if (Input::get('lesson_id', 0) > 0) {
                $lesson = Lesson::where('id', '=', Input::get('lesson_id', 0))->first();
                $lesson->test_id = $test_id;
                $lesson->save();
             }
 
            if (Input::get('course_id', 0) > 0) {
                $course = Course::where('id', '=', Input::get('course_id', 0))->first();
                    if ($course->test_id == 0){
                        $course->test_id = $test_id;
                        $course->save();

                        $lesson = new Lesson;
                        $lesson->lesson_name = 'Итоговый тест';
                        $lesson->course_id = $course->id;
                        $lesson->lesson_description = '';
                        $lesson->test_id = $test_id;
                        $lesson->save();
                    }
                    else{
                        $course->test_id = $test_id;
                        $course->save();
                    }

            }


            return 'ok';
        }
        catch (\Exception $e) {}

        return 'wrong format';
    }


    public function getDetails($test_id = 0)
    {
        if (!Auth::user() || !Auth::user()->is_admin){
            return redirect('/');
        }

        $attempts = Attempt::join('tests', 'tests.id', '=', 'attempts.test_id')
            ->leftJoin('users', 'users.id', '=', 'attempts.user_id')
            ->where('test_id', '=', $test_id)
            ->where('is_completed', '=', true)
            ->select(['attempts.*', 'tests.title', 'tests.questions', 'users.email'])
            ->orderBy('attempts.created_at')
            ->get()
            ->groupBy('user_id');

        $test = Test::where('id', '=', $test_id)->first();

        $this->set('attempts', $attempts)->set('test', $test);
    }


    static function delete_files($target) {
        if (is_dir($target)) {

            $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

            foreach( $files as $file )
            {
                self::delete_files( $file );
            }

            rmdir( $target );

        } elseif (is_file($target)) {
            unlink( $target );
        }
    }

}