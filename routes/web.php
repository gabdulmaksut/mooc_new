<?php



Route::get('auth/login', 'AuthController@getLogin');
Route::post('auth/login', 'AuthController@postLogin');
Route::get('auth/logout', 'AuthController@getLogout');


Route::get('auth/reg', 'AuthController@getReg');
Route::post('auth/reg', 'AuthController@postReg');
Route::get('auth/password-change-request', 'AuthController@getPasswordChangeRequest');
Route::post('auth/password-change-request', 'AuthController@postPasswordChangeRequest');
Route::get('auth/password-change/{code}', 'AuthController@getPasswordChange');
Route::post('auth/password-change/{code}', 'AuthController@postPasswordChange');
Route::get('auth/activate/{code}', 'AuthController@getActivate');
Route::post('auth/activate/{code}', 'AuthController@postActivate');

Route::post('ajax/resend-mail', 'AjaxController@postResendMail');

Route::get('/', 'WelcomeController@getIndex');
Route::post('/', 'WelcomeController@postIndex');


Route::group(['middleware' => 'auth'], function () {

    Route::get('users/{role?}', 'UsersController@getIndex');

    Route::get('users/editteacher/{id}', 'UsersController@getEditTeacher');
    Route::post('users/editteacher/{id}', 'UsersController@postEditTeacher');

    //  Route::get('users/delete/{id}', 'UsersController@getDelete');
    //  Route::post('users/delete/{id}', 'UsersController@postDelete');
    Route::get('users/admin/{id}', 'UsersController@getAdmin');
    Route::post('users/admin/{id}', 'UsersController@postAdmin');
    Route::post('users/teacher/{id}', 'UsersController@postTeacher');

    Route::get('tests', 'TestsManagerController@getIndex');
    Route::get('tests/delete/{id}', 'TestsManagerController@getDelete');
    Route::post('tests/delete/{id}', 'TestsManagerController@postDelete');
    Route::post('ajax/delete-test', 'AjaxController@postDeleteTest');
    Route::post('ajax/delete-term', 'AjaxController@postDeleteTerm');

    Route::get('tests/load', 'TestsManagerController@getLoad');
    Route::post('tests/load', 'TestsManagerController@postLoad');

    Route::get('tests/details/{id}', 'TestsManagerController@getDetails');
    Route::post('tests/details/{id}', 'TestsManagerController@postDetails');

    Route::get('profile', 'ProfileController@getIndex');
    Route::post('profile', 'ProfileController@postIndex');

    Route::get('profile/{id}', 'ProfileController@getIndex');
    Route::post('profile/{id}', 'ProfileController@postIndex');


    Route::get('/testslist', 'TestsListController@getIndex');
    Route::post('/testslist', 'TestsListController@postIndex');    

    Route::get('/test/preview/{test_id}', 'TestController@getIndex');
    Route::post('/test/preview/{test_id}', 'TestController@postIndex');

    Route::get('/test/result/{test_id}/{attempt_id}', 'TestController@getResult');
    Route::post('/test/result/{test_id}/{attempt_id}', 'TestController@postResult');

    Route::get('/test/{test_id}/{question}', 'TestController@getTest');
    Route::post('/test/{test_id}/{question}', 'TestController@postTest');

    Route::post('ajax/start-attempt', 'AjaxController@postStartAttempt');
    Route::post('ajax/save-answer', 'AjaxController@postSaveAnswer');
    Route::post('ajax/end-attempt', 'AjaxController@postEndAttempt');
    //web.php
    Route::get('courses', 'CoursesController@getIndex');
    Route::post('courses', 'CoursesController@postIndex');

    Route::get('/courses/create', 'CoursesController@getCreateCourse');
    Route::post('/courses/create', 'CoursesController@postCreateCourse');

    Route::get('/courses/edit/{id}', 'CoursesController@getEdit');
    Route::post('/courses/edit/{id}', 'CoursesController@postUpdate');
    Route::post('/courses/delete/{id}', 'CoursesController@postDelete');

    Route::get('/courses/allcourses', 'CoursesController@getAllcourses');

    //Route::get('/courses/students/', 'CoursesController@getStudents');
    //Route::post('/courses/students/', 'CoursesController@posStudents');

    Route::get('/courses/mycourses/', 'CoursesController@getMyCourses');
    Route::post('/courses/mycourses/', 'CoursesController@postMyCourses');

    Route::get('/courses/mystudents/{id?}', 'CoursesController@getMyStudents');
    Route::post('/courses/mystudents/{id?}', 'CoursesController@postMyStudents');

    Route::get('chat', 'ChatController@getIndex');
    Route::post('chat', 'ChatController@postIndex');

    Route::post('sendmessage', 'ChatController@postSendMessage');
    Route::post('takeold', 'ChatController@postTakeOld');
    Route::post('setread', 'ChatController@postSetRead');   
 
    Route::get('/lessons/{id}', 'LessonsController@getLessons');
    Route::post('/lessons/{id}', 'LessonsController@postLessons');

    Route::get('/lessons/edit/{course_id}/{lesson_id}', 'LessonsController@getEdit');
    Route::post('/lessons/edit/{course_id}/{lesson_id}', 'LessonsController@postEdit');
    Route::post('/lessons-delete', 'LessonsController@postDelete');
    Route::post('/lesson-visit', 'LessonsController@postVisit');

    Route::get('/categories/allcategories/', 'CategoriesController@getAllCategories');
    Route::post('/categories/allcategories/', 'CategoriesController@postAllCategories');

    Route::get('/categories/edit/{id}', 'CategoriesController@getEdit');
    Route::post('/categories/edit/{id}', 'CategoriesController@postEdit');

    Route::get('/categories/create', 'CategoriesController@getCreateCategory');
    Route::post('/categories/create', 'CategoriesController@postCreateCategory');

    Route::get('/lesson/{course_id}/{lesson_id}', 'LessonsController@getLesson');
    Route::get('/lesson-content/{course_id}/{lesson_id}', 'LessonsController@getLessonContent');


    Route::post('/expand', 'AjaxController@postExpand');


});

     Route::get('/categories/{id?}', 'CategoriesController@getIndex');
     Route::post('/categories/{id?}', 'CategoriesController@postIndex');
     Route::get('/course/{id}', 'CategoriesController@getCourse');
     Route::post('/course/{id}', 'CategoriesController@postCourse');

