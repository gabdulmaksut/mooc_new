<?php

namespace DevDojo\Chatter\Controllers;

use Auth;
use DevDojo\Chatter\Helpers\ChatterHelper as Helper;
use DevDojo\Chatter\Models\Models;
use Illuminate\Routing\Controller as Controller;
use App\Models\MyCourses;
use App\Models\ChatterCategory;
use App\Models\Course;

class ChatterController extends Controller
{

    public function index($slug = '')
    {
        $pagination_results = config('chatter.paginate.num_of_results');

        $my_courses = [];
        if (Auth::user()->role == 1){
            $my_courses = MyCourses::where('user_id', '=', Auth::user()->id)->pluck('course_id')->toArray();
        }
        if (Auth::user()->role == 2){
            $my_courses = MyCourses::where('user_id', '=', Auth::user()->id)->pluck('course_id')->toArray();
            $my_courses = Course::whereIn('id', $my_courses)->orWhere('user_id', '=', Auth::user()->id)->pluck('id')->toArray();
        }
        if (Auth::user()->role == 3){
            $my_courses = Course::pluck('id')->toArray();
        }

        $my_chatter_categories = ChatterCategory::whereIn('order', $my_courses)->pluck('id')->toArray();

        $discussions = Models::discussion()->whereIn('chatter_category_id', $my_chatter_categories)->with('user')->with('post')->with('postsCount')->with('category')->orderBy(config('chatter.order_by.discussions.order'), config('chatter.order_by.discussions.by'));
        if (isset($slug)) {
            $category = Models::category()->where('slug', '=', $slug)->first();
            
            if (isset($category->id)) {
                $current_category_id = $category->id;
                $discussions = $discussions->where('chatter_category_id', '=', $category->id);
            } else {
                $current_category_id = null;
            }
        }
        
        $discussions = $discussions->paginate($pagination_results);

        $categories = Models::category()->whereIn('order', $my_courses)->get();
        $categoriesMenu = Helper::categoriesMenu(array_filter($categories->toArray(), function ($item) {
            return $item['parent_id'] === null;
        }));
        
        $chatter_editor = config('chatter.editor');
        
        if ($chatter_editor == 'simplemde') {
            // Dynamically register markdown service provider
            \App::register('GrahamCampbell\Markdown\MarkdownServiceProvider');
        }

        \Carbon\Carbon::setLocale('ru');

        $_section = 'forums';
        $_view = 'home';
        $_user = Auth::user();
        $_metadata['title'] = "Онлайн консультации";

        return view('chatter::home', compact('discussions', 'categories', 'categoriesMenu', 'chatter_editor', 'current_category_id', '_user', '_section', '_view', '_metadata'));
    }
    
    public function login()
    {
        if (!Auth::check()) {
            return redirect('auth/login')->with('login_error', "Пожалуйста, залогиньтесь.");
        }
    }
    
    public function register()
    {
        if (!Auth::check()) {
            return redirect('auth/login')->with('login_error', "Пожалуйста, залогиньтесь.");
        }
    }
}
