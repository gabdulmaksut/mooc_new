
@if( $chatter_editor == 'tinymce' || empty($chatter_editor) )
    <script src="{{ url('/vendor/devdojo/chatter/assets/vendor/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ url('/vendor/devdojo/chatter/assets/js/tinymce.js') }}"></script>
    <script>
        var my_tinymce = tinyMCE;
        $('document').ready(function(){
            $('#tinymce_placeholder').click(function(){
                my_tinymce.activeEditor.focus();
            });
        });
    </script>
@elseif($chatter_editor == 'simplemde')
    <script src="{{ url('/vendor/devdojo/chatter/assets/js/simplemde.min.js') }}"></script>
    <script src="{{ url('/vendor/devdojo/chatter/assets/js/chatter_simplemde.js') }}"></script>
@elseif($chatter_editor == 'trumbowyg')
    <script src="{{ url('/vendor/devdojo/chatter/assets/vendor/trumbowyg/trumbowyg.min.js') }}"></script>
    <script src="{{ url('/vendor/devdojo/chatter/assets/vendor/trumbowyg/plugins/preformatted/trumbowyg.preformatted.min.js') }}"></script>
    <script src="{{ url('/vendor/devdojo/chatter/assets/js/trumbowyg.js') }}"></script>
@endif

<script src="{{ url('/vendor/devdojo/chatter/assets/js/chatter.js') }}"></script>
<script>
    $('document').ready(function(){

        $('.chatter-close, #cancel_discussion').click(function(){
            $('#new_discussion').slideUp();
        });
        $('#new_discussion_btn').click(function(){
            @if(Auth::guest())
                window.location.href = "{{ url('forums/login') }}";
            @else
                $('#new_discussion').slideDown();
                $('#title').focus();
            @endif
        });

        @if (count($errors) > 0)
            $('#new_discussion').slideDown();
            $('#title').focus();
        @endif


    });
</script>