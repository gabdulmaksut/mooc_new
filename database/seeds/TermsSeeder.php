<?php

use Illuminate\Database\Seeder;

class TermsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared("TRUNCATE `terms`");
        DB::unprepared("INSERT INTO `terms` (id, course_id, term, definition)
		  VALUES
(1,1, 'Абиотикалық орта', 'өлі табиғаттың барлық факторларының кешені.'),
(2,1, 'Абиотикалық факторлар', '(гр. \'a\' – теріс және bіotіkos – тірішілік, өмір) –бейорганикалық ортаның тірі организмдерге жасайтын тікелей немесе жанама әсерлерінің жиынтығы; сыртқы ортаның бейорганикалық, физикалық және химиялық жағдайлары.'),
(3,1, 'Автотрофты ағзалар', '(гр. Autos – өзі және trophй – қорек) аутотрофты организмдер – қоршалған ортадағы бейорганикалық заттардан фотосинтез немесе хемосинтез процесі нәтижесінде тіршілігіне қажетті органикалық зат түзетін организмдер; химиялық реакциялар барысында босайтын энергияларды немесе сәуле энергиясын пайдалана отыра органикалық емес қоспалардан органикалық заттарды синтездеуші ағзалар.'),
(4,1, 'Адаптация', '(лат. Adaptatio — бейімделу) — жануарлар организмдерінің, олардың мүшелер жүйелерінің құрылысы мен қызметі жағынан белгілі бір тіршілік ортасына бейімделу процесі; бір биологиялық түрдің морфофизиологиялық мінез-құлық популяциялық т.б. оған ыңғайлы табиғи ортада өзіне тән өмір сүруіне мүмкіндік беретін ерекшіліктері.'),
(5,1, 'Аллогендік сукцессиялар', 'сырттан әсер ететін геохимиялық күштер және антропогендік процестер.'),
(6,1, 'Анабиоз', '(гр.anabіosіs – жандану, қайта тірілу) – қолайсыз жағдайда организмнің тіршілік қабілетінің уақытша жойылып, не мүлдем әлсіреп кету құбылысы немесе эволюциялық даму жолында организмнің тіршілік ортасына физиологиялық бейімделуі; уақытша күй, мұнда өміршендік процесстердің баяулатылғандығы соншалық тіршіліктің барлық құбылыстары толығымен байқалмайды.'),
(7,1, 'Антофилдер', 'тозаңдардағы шырындықтарды пайдаланатындар.'),
(8,1, 'Антропогендік фактор', '(грек. anthropos – адам, genos – тегі, пайда болуы, лат. factor – іс-әрекет) – адамның барлық тірі организмдердің мекен ортасы ретіндегі табиғатты өзгертуіне әкеп соғатын немесе олардың тіршілігіне тікелей әсер ететін сан алуан әрекеттері.'),
(9,1, 'Ареал', '(лат.area– алаң, кеңістік) – өсімдіктер мен жануарлар түрінің, туысының, тұқымдасының құрлықта не судағы таралған аймағы; берілген таксон таралып және оның дамуынын толық циклі өтетеін жердің немесе теңіздің беті (түр, топ, қатар және т.б.). Ареал онда ағзалар соның бойымен немесе бірқалыпты орналасса ол тұтас болады, егер ағзалардың таралуы екі немесе бірнеше бір-бірінен оқшауланған аймақтармен шектелсе онда бөлінген болады.'),
(10,1, 'Ареалогия', 'ареалдар жөніндегі биогеографияның маңызды салаларының бірі.'),
(11,1, 'Ареалогиялық биогеография', ' жер шарындағы әр түрлі организмдердің (түрлердің) таралу аймағын анықтап, олардың сол аймақтардағы орналасу ерекшеліктерін зерттейді, соның негізінде анықтамалық және кадастрлық карталар жасалады;'),
(12,1, 'Ареалдық-минимум', 'өсімдіктер қауымдастықтарындағы жиі кездесетін түрлерді тіркеуге арналған кішкене аумақ.'),
(13,1, 'Аридтік бедер', 'шөлдерге, шөлейттерге және күрғақ далаларға тән бедердің типі. Золдық әрекеттердің, үгілудің, жазықтық шайылудың, мезгілдік ағын сулардың және тағы да басқалары өсерінен қалыптасады.'),
(14,1, 'Антропогенез', '(грек. anthropos – адам, genesіs – шығу тегі) – антропология ғылымының адамның шығу тегін, даму тарихын, оның жеке биол. түр болып қалыптасуын және адамзат қоғамының даму кезеңдерін әрі жаратылыстану, әрі қоғамдық ғылымдарға сүйене отырып зерттейтін негізгі саласы.'),
(15,1, 'Атмосфера', 'жердің ауа қабығы.'),
(16,1, 'Бедер', 'көрінісі, мөлшері, пайда болуы, жасы және даму тарихы жағынан әр түрлі жер беті пішіндерінің жиынтығы.Қыраттарды түзетін оң пішіндерді және ойпандардан тұратын теріс пішіндерді құрайды. Масштабына карай мынадай топтарға бөледі: планеталық (материктер мен мұхит шарасы), мегапішіндер (таулы жүйелер, жазық өңірлер), макропішіндер (тау жоталары, тауаралық ойпаңдар), мезопішіндер (төбелер, өзен аңғарлары, сайлар), микропішіндер (эрозиялық жырмалар, карст шұңқырлары), нанопішіндер (шалғындық томарлар, кеміргіштердің індері, жел иірімдері).'),
(17,1, 'Биогендік заттар', 'су объектілерінде фитопланктондардың жылдам өсіп-өнуіне зиянкес су ағзаларының өсуіне, су көзінің эвтрофиялық дамуына себепші болып, судың өздігінен тазару процесіне кері ықпалы болатын заттар.'),
(18,1, 'Биогеография', 'жер шарындағы өсімдіктер организмінің таралуы мен олардың өр жердегі топтануын, табиғи жағдайы мен геологиялық тарихына байланыстыра зерттейтін ғылым.'),
(19,1, 'Биогеоценоз', '(био..., гео... және гр. Koіnos –  жалпы) – тіршілік ету жағдайлары ұқсас, белгілі аумақта өсетін өзара байланысты түрлердің (популяциялардың) тыныс-тіршілік ортасы.'),
(20,1, 'Биогеографиялық фация', 'тіршілік жағдайында бір биотоп шегінде болмашы ерекшеліктерімен сипатталатын үлескілер.'),
(21,1, 'Биогеосфера',' Жер шарының тіршілік шоғырланған қабығы; гидросфера мен атмосфераның, жер беті қабығының, литосфераның жапсарласкан тұсында орналасқап. Биогеосфера биосфера биосферамен салысырғанда толымсыздау ұғым. Биогеосфера адам тұрақты мекен ететін және қалыпты өмір сүретін Жердің бірегей қабаты болып табылады. Терминді орыс ғалымы Ю.К. Ефремов ұсынды (1959). Биосфераның бірнеше синонимі бар: тіршілік қабыршағы (В.И. Вернадский), биогеоценоздық жамылғы(В.Н. Сукачев), фитогеосфера(Е.М. Лавренко), эпигенема (Р.И. Або- лин), витосфера (А.Н. Тюрюканов және В.Д. Александрова). '),
(22,1, 'Биомасса', '(гр. Bios- өмір және масса) – бір түрдің, түрлер тобының немесе бүтіндей бірлестіктердің (өсімдік, микроорганизм және жануарлардың) тіршілік ететін мекенінің бірлік бетіне не көлеміне келетін жалпы массасы; аудан немесе көлем (г/м2 немесе г/м3) бірлігіне салмағы бойынша өрнектелген тірі ағзалар мөлшері.'),
(23,1, 'Бентос',  'су қоймаларының түбіндегі организмдер жиынтығы.'),
(24,1, 'Биологиялық алға басу (прогресс) дегеніміз', 'дарақтар санының, жүйеленген. сан алуандыцтың (едәуір усақ жүйеленген. топтар санының) артуы және аймақтың кеңеюі.'),
(25,1, 'Биом',' (био... лат. omat, oma — жиынтық, жинақталған, біріккен деген мағынаны білдіретін жалғау) — белгілі бір ландшафтылық-географиялық аймақта мекендейтін өсімдіктер мен жануарлар түрлері мен олардың тіршілік ету ортасының жиынтығы (мысалы, тундра, қылқанжапырақты орманды алқап, т.б.)'),
(26,1, 'Биосфера','(гр. Биос – тіршілік, өмір, гр. сфера – шар) бұл ұғым биология ғылымына XIX ғасырда ене бастады. Ол кездерде бұл сөзбен тек жер жүзіндегі жануарлар дүниесі ғана аталатын. Кейінгі кездерде биосфера геологиялық мағынада да қолданылады. Биосфера — тірі азғалар өмір сүретін жер қабаты.'),
(27,1, 'Биотикалық фактор', 'биогендік фактор – ағзалардың тіршілік әрекетіне байланысты бір-біріне тигізетін сан алуан әсерлері. Биотикалық фактордың абиотикалық факторданайырмашылығы, мұнда әр түрге жататын азғалар бір-біріне өзара және айналадағы ортаға әсерін тигізеді.'),
(28,1, 'Биоценоз', '(био... және гр. Koіns –  жалпы) – тіршілік жағдайлары азды-көпті біркелкі орта өңірін мекендейтін жануарлардың, өсімдіктер мен микроорганизмдердің жиынтығы.'),
(29,1, 'Биоценология', '(биоценоз және гр. logos - ғылым) – тірі организмдердің құрылысын, дамуын, табиғатта орналасуын, пайда болу мерзімін, шыққан арғы тегін зерттейтін ғылым'),
(30,1, 'Биполярлық ажыраулар', 'ареал бөліктері солтүстік және оңтүстік жарты шарының полярлық аудандарында жатыр.'),
(31,1, 'Географиялык ландшафт', 'құрамындағы табиғат құраушылары (жер бедері, климат, су, топырақ, өсімдік бірлестіктері мен жануарлар) мен морфологиялық бөліктері (фация, қоныс, жергілікті жер) өзара үйлескен, өзіндік құрылымы бар, географиялық қабықтың салыстырмалы түрдегі біртектес бөлігі. “Ландшафт” (нем.land – жер,schaft – өзара байланысты білдіретін жұрнақ) терминін орыс ғалымы Л.С. Берг енгізген. Географиялык ландшафтқа тән негізгі көрсеткіштер қатарына аумақтың біркелкі сипаты, құраушыларының біртекті ұштасуы, құрылымының кешенді сипаты мен біртұтастығы, тұрақтылығы, зат және энергия алмасуының біртектестігі жатады.'),
(32,1, 'Гетеротрофты ағзалар', '(грек. heteros – басқа, жат, trophe – қорек) – негізінен дайын органик. заттармен қоректенетін организмдер. Олар өз денесінің құрамын бейорганик. заттардан түзе алмайды.'),
(33,1, 'Геоэкология', ' жер бетіндегі экожүйелердегі және биосфералық деңгейдегі сыртқы орта құбылыстарының өзара байланысын және олардың тірі азғалармен қарым-қатынасын зерттейді.'),
(34,1, 'Гидрофиттер', '(гр. Hydo – су, рһуton – өсімдік) – су табанындағы топыраққа бекітілген және суға тек төменгі бөлігімен батқан су өсімдіктері.'),
(35,1, 'Гидросфера', '(гр.һуdor – су, spһаіrа – шар) –  жер ғаламшарының су қабығы немесе құрлықтағы (тереңдегі, топырақтағы, жер бетіндегі), мұхиттағы және атмосферадағы, яғни жер шарындағы барлық сулардың жиынтығы. Оны мұхиттар мен теңіздердің суы, құрлық сулары – өзендер, көлдер, бөгендер, мұздықтар, сондай-ақ литосфераның жоғарғы бөлігіне сіңетін жер асты суы, атмосферадағы ылғал құрайды.'),
(36,1, 'Гидросфераның ластануы', 'ластағыш заттардың гидросфераға мол мөлшерде түсуі, олардың өзен, су қоймалары, көлдер мен теңіздер, мұхиттар мен жер асты суларын ластап, су ортасының қалыпты жағдайын бұзуы.'),
(37,1, 'Дегрессия', 'биоценозға антропогендік әсерлер негізінде экожүйенің азаюы, жайдақтануы.'),
(38,1, 'Жасанды биогеоценоз', 'түрлі агрономиялық әдістерді қолдану нәтижесінде алынған агроценоздар. Бұған қолдан жасалатын шалғындықтар, егістіктер мен жайылымдар, қолдан отырғызылатын ормандар жатады.'),
(39,1, 'Зоопаразиттер', 'жануарлардың денесінде өмір сүретін және олардың шырындармен қоректенетіндер.'),
(40,1, 'Зоогеография', 'Жер шарындағы жануарлардың белгілі бір аймақта таралуын, олардың тіршілік жағдайларын, түр құрамын, дамуын зерттейтін биогеографиялық ғылымының бір саласы. Зоогеография ғылымының негізі 18 ғасырда қаланды. Құрлықтағы организмдерді зоогеографиялық тұрғыда жіктеуді Н.А.Северцов пен М.А.Мензбир экологиялық және тарихи тұрғыдан қарастырған. Зоогеография(грекше: zoon - жануар және география) – Жер шарындағы жануарлар дүниесінің таралуы мен орналасу заңдылықтарын зерттейтін ғылым;биогеографияның бөлімі.'),
(41,1, 'Зоофагтар', 'қорек ретінде тірі жануарларды пайдаланады.'),
(42,1, 'Индикаторлар', 'жерастындағы кендерді анықтауға жәрдемдесетін өсімдіктер.'),
(43,1, 'Климатоп', '(грекше klima (klimatos) еңіс, topos – орын) – ортаның (ауа немесе сулы ортаның, олардың газдық құрамының, температуралық режимінің және т.б.) және бұл ортада тіршілік ететін организмдерге тон физикалық және химиялық сипаттамаларының үйлесімі.'),
(44,1, 'Консументтер', '(лат. consumo – тұтынамын), тұтынушылар – қоректік тізбекте фотосинтез немесе хемосинтез жүргізетін өндіргіштер (продуценттер) түзетін дайынорганикалық заттарды пайдаланатын организмдер. Барлық гетеротрофты организмдер Консументтер болып табылады. Олар өздері пайдаланған органикалық заттарды ақырғы өнімдерге дейін ыдыратпайды. Консументтер тобына барлық адам, жануарлар түрі, микроорганизмдердің біраз тобы, паразит және жәндік жегіш өсімдіктер жатады.'),
(45,1, 'Консорция', '(латынша consortium қатысу) – өзара тығыз байланысқан және қауымдастықтың орталық мүшесіне немесе ядросына тәуелді әр текті организмдер жиынтығы.'),
(46,1, 'Көміртек айналымы', 'табиғатта көміртектің үздіксіз айналу құбылысы.'),
(47,1, 'Көші - қон', 'жануарлар ортасының қолайсыз жағдайларына не олардың даму циклдеріне байланысты жылжып қозғалуын атайды.'),
(48,1, 'Күкірт айналымы', 'табиғатта күкірттің үздіксіз айналу құбылысы.'),
(49,1, 'Қалалық экожүйе', 'қала аумағы және оның тұрғындары (адам және басқа тірі организмдер).'), 
(50,1, 'Қысқа күн өсімдіктері', 'тропик және субторпик өсімдіктері.'),
(51,1, 'Мангр (Mangrove)', 'мәңгі жасыл ағаштар мен бұталар өскен ормандар. Мангр ормандары тропиктік белдеуде және теңіз (мұхит) жағалауларында өседі. Ағаштардың биіктігі негізінен 10 м-ден аспайды. Флорасында ризофорлар мен вербендер (нар-қайсар) басым өскен.'),
(52,1, 'Метабиоз', 'микроағза түрлері арасындағы, олардың бір түрінің тіршілік өнімдері, екіншісінің қорек көзіне айналуымен сипатталатын, олардың өзара қатынастыққалыптары.'),
(53,1, 'Мемлекеттік табиғи қорық қоры', 'қоршаған ортаның табиғи эталондар, реликтілері, ғылыми зерттеулерге, ағарту білім беру ісіне, туризмге және рекреацияға арналған нысандары ретінде экологиялык, ғылыми және мәдени жағынан ерекше құнды, мемлекеттік қорғауға алынған аумақтарының жиынтығы. Қорықтардың басты мақсаты – табиғи ландшафтылар эталонын мұндағы тіршілік ететін өсімдіктер мен жануарлар дүниесімен коса сақтау, табиғат кешендерінің табиғи даму заңдылықтарын анықтау.'),
(54,1, 'Микроорганизмдер', 'микробтар – тек қана микроскоппен көруге болатын өте ұсақ организмдер.'),
(55,1, 'Мелиорациялық аудандастыру', 'жерді аудандастыру, табиғи кешендік факторлар арқылы, мелиорацияның қажеттілігін және оның іске асырылу мүмкіндігін анықтау.'),
(56,1, 'Льяностар', 'Оңтүстік Америка да, Гвианнадағы саванналар.'),
(57,1, 'Палеозоогеография', 'географияның ерте кезде тіршілік еткен жануарлардың таралуын зерттейтін бөлім.'),
(58,1, 'Пантропикалық ажыраулар', 'барлық континенттер мен архипелагтар тропикасында жататын ареал бөліктері.'),
(59,1, 'Планктон', '(гр. Plаnktos – кезбе) – теңіздің, су айдындарының әр түрлі тереңдігінде мекендейтін және суда қалқып тіршілік ететін, су ағынына төтеп бере алмайтын организмдер жиынтығы.'),
(60,1, 'Патшалық', 'флоралық аудандастырудың жоғары бірлігі.'),
(61,1, 'Продуценттер', 'өз ағзаларын өздері қалыптастыратын организмдер'),
(62,1, 'Популяция', '(латын тілінде populus – халық, тұрғын халық) – белгілі бір кеңістікте генетикалық жүйе түзетін, бір түрге жататын және көбею арқылы өзін-өзі жаңғыртып отыратын азғалар тобы.'),
(63,1, 'Прерия', '(француз тілінде praіrіe, латын тілінде partum – шалғындық) Солтүстік Американың орталығы бөлігіндегі үстіртті жазық өңір. Теңіз деңгейінен 500 – 1500 м биіктікте.'),
(64,1, 'Редуценттер', '(лат. Reducentіs – қалпына келтіруші), ыдыратушылар – өлі органикалық заттарды (өлекселер мен организм қалдықтарын) ыдыратып, оларды органикалық емес заттарға айналдыратын организмдер (сапротрофтар). Оларды кейде деструкторлар, яғни ыдыратушылар деп те атайды.'),
(65,1, 'Реликт', '(лат. relіctum –  қалдық) – геологиялық замандарда кеңінен таралған, осы кезде тек шағын алқаптарда сақталған өсімдіктер мен жануарлар түрлері немесе табиғи бірлестіктер. Реликттер өздері тіршілік еткен заманның жасымен: мыс., палеозой заманымен (латимерия), мезозой заманымен (гаттерия,гинкго), кайнозойзаманымен (секвойя, қортық қайың, т.б.) ерекшеленеді. Кайнозойдың мұздану немесе мұзданудан кейінгі кезеңінен сақталған Реликт өсімдіктер мен жануарларпалеонтологиялық Реликтілер деп аталады.'),
(66,1, 'Саванна', '(ис. sabana - кариб үндістерінің тілінен енген) – тропиктік ормандар мен шөлдер аралығындағы биом түрі.'),
(67,1, 'Синузия', '(грек, synisia – бірлесіп болу, қауымдастық) – мекен ортасына ортақ талаптарымен өзара байланысқан, көбіне құрауыш организмдердің жиынтығының өзі едәуір дәрежеде түрін өзгерткен организмдер популяцияларының жиынтығы.'),
(68,1, 'Суккуленттер', 'сабақтары жуан да етті және жапырағында мол су қоры бар әр түрлі тұқымдас өсімдіктер тобы. Жапырақта жиналған су қорын Өсімдік қуаңшылық кезеңде пайдаланады. Суккуленттер шөлдер мен шөлейттерде тараған. Мысалы, кактустар, сүттігендер тағы басқалар.'),
(69,1, 'Сукцессия', '(лат.succesіo – сабақтастық, біртіндеп ауысу) – Жер бетінің белгілі бір аймағындағы бір биогеоценоздың екіншісіне ауысуы. Сукцессия нәтижесінде биогеоценоздардың өзгеруі заңды құбылыс, ол организмдердің бір-бірімен және оларды қоршаған ортаның абиотиктердің факторларының арасындағы өзара қарым-қатынасы арқылы жүзеге асады. Сукцессия белгілі бір уақыт аралығын қамтиды.'),
(70,1, 'Табиғи биогеоценоз', '(тоған, орман, т.б.) – табиғи сұрыпталу нәтижесінде қалыптасатын өздігінен реттелетін күрделі де тұрақты биологиялық жүйе.'),
(71,1, 'Тірі организмдер дүниесі', 'құрылымы және биосферадағы функциялық рөлі жөнінен тірі организмдердің ең ірі бірлестіктері тірі организмдер дүниесінің 5 түрі сараланады.'),
(72,1, 'Тіршілік ортасы', '(Биотическая среда) – қазіргі тіршілік етіп жатқан организмдердің арқасында пайда болған табиғи күштер мен құбылыстар. Тіршілік ортасы деп белгілі бір ағза (түрдің) дамуының барлық кезендеріне қажетті табиғи орта факторлар жиынтығын айтады.'),
(73,1, 'Тропосфера', '(грек.τροπόσ – бұрылыс, өзгеріс және σφαιρα – шар) –атмосфераның жер бетімен тығыз әрекетте болатын ең төменгі қабаты. Тропосфера түгелдей Жердің географиялық қабығының құрамына кіреді.'),
(74,1, 'Фауна', '(лат. Fauna – Ежелгі Рим мифологиясы бойынша орман мен егістік құдайы; жануарлар қамқоршысы) – белгілі бір аумақта мекендейтін не Жер тарихының белгілі бір кезеңінде тіршілік еткен барлық жануарлар түрлерінің жиынтығы.'),
(75,1, 'Фауналық аудандастыру', 'белгілі бір аумақта таралған жануарлар дүниесінің түр құрамына, олардың тарихи қалыптасуына, ондағы эндемик түрлердің сақталып қалуына және түрлердің таралу заңдылықтарына байланысты жүрңгізіледі.'),
(76,1, 'Фация', '(лат. facіes – келбет, түр) – бір стратиграфиялық горизонттың таралу аумағындағы тау жыныстарының қалыптасу жағдайы. Фация терминін Швейцария геологы А.Грессли ұсынған (1941). Бір зерттеушілер Фацияны жер қыртысының физ.-геогр. жағдайлары және шөгіндінің фаунасы мен флорасыбірдей бөлікшесі деп түсіндірсе, басқалары шөгінді тау жыныстары түзілу ортасының палеонтол. және петрограф. ерекшеліктерінің кешені деп есептейді. Фацияларды анықтау барысында тау жыныстары сипаттамасының бірнеше басты өлшемдері негізге алынады.'),
(77,1, 'Фитобентос', 'өсімдіктер  бентосы (фитобентос) тайыз жағалау зонада орналасады.'),
(78,1, 'Фитомасса', '(грек сөзі phyton – өсімдік және масса) – бір ғана биоценоздағы немесе экожүйелердегі өсімдіктердің биомассалары.'),
(79,1, 'Фитопланктон', 'микроскопиялық жасыл өсімдіктер, негізінен, балдырлар, сондай-ақ, су қабатында еркін жүзіп жүретін кейбір жоғары өсімдіктер.'),
(80,1, 'Фитоценоз', '(фито… және ценоз), өсімдіктер қауымдастығы – құрлықтың біртекті телімдерінде орналасқан, өзара және сыртқы ортамен тығыз әрекетте болатын өсімдіктер жиынтығы. Олардың өздеріне тән құрылысы бар және сұрыптау нәтижесінде бір-бірімен және басқа организмдермен бірге белгілі бір жағдайда тіршілік ете алатын түрлерден құралады. Фитоценоз – ашық биология жүйе, яғни, адам мен гетеротрофты жануарларға қажетті органиктік зат өндірілетін биогеоценоздың негізгі бөлігі болып саналады.'),
(81,1, 'Фитохорология', 'өсімдік географияның өсімдіктің түрін, туысын тағы басқа таксономиялық котегорияларының табиғатта таралу аймағын-ареалын зерттейтін саласы.'), 
(82,1, 'Флора', '(латынша flora – Рим мифологиясындағы көктем құдайы; латынша florіs — гүл) – нақты бір жер аумағында өсетін немесе өткен геологиялық дәуірлерде өскен өсімдік таксондарының тарихи-эволюциялық қалыптасқан жиынтығы. Флораны өсімдіктер бірлестігінен (әр түрлі өсімдік қауымдастығының жиынтығынан) ажыратып қарау қажет.'),
(83,1, 'Флуктуация', '(латынша fіuctuatіo – үздіксіз қозғалыс, тербеліс) – физикалық шамалардың (бақыланатын мәндері) өздерінің орташа мәндерінен кездейсоқ ауытқуы. Бұл құбылысты зерттеудің принциптік маңызы бар.'),
(84,1, 'Фототропизм', 'жарыкқа бағытталып өсімдік мүшелерінің бір бағыттан түскен жарыққа карай бұрыла өсуі.'),
(85,1, 'Эволюция', '(лат. evolutіo – өрлеу, өркендеу), биологияда – тірі табиғаттың қайта айналып келмейтін және тура бағытталған тарихи дамуы.'),
(86,1, 'Экология', '( лат. оіkos – үй, баспана; logos – ілім) – жеке организмнің қоршаған ортамен қарым-қатынасын, ортаға бейімделу заңдылықтарын, сондай-ақ организм деңгейінен жоғарырақ тұрған биологиялық жүйелердің – популяциялардың, организмдер қауымдастықтарының, экожүйелердің, биосфераның ұйымдастырылу және қызмет атқару заңдылықтарын зерттейтін ғылым.'),
(87,1, 'Экологиялық жүйе, экожүйе', 'тірі ағзалар жиынтығының қоректену, өсу және ұрпақ беру мақсатында белгілі бір тіршілік ету кеңістігін бірлесе пайдалануының тарихиқалыптасқан жүйесі. Экожүйе құрамына организмдер де, табиғи орта да кіретін тірі табиғаттың негізгі функционалдық бірлігі болып табылады. Экожүйенің құрылымынэнергияны трансформациялаудың үш деңгейі (консументтер, продуценттер, редуценттер) мен қатты және газ тәрізді заттар айналымы құрайды. Экожүйенің қасиеттері оның құрамына кіретін өсімдіктер мен жануарлардың әрекеттеріне байланысты.'),
(88,1, 'Экологиялық пирамидалар', 'экожүйедегі продуценттер, консументтер (бірінші және екінші реттік) және редуценттер арасындағы олардың массасымен өрнектелген арақатынас (санмен өрнектелуі – Элтон сандарының пирамидасы, құрамындағы энергияға қатысты – энергиялар пирамидасы).'),
(89,1, 'Экологиялық тепе-теңдік', 'кез келген табиғи қауымдастықтағы тірі организмдердің түрлік құрамының, олардың санының, өнімділігінің, кеңістікте бөлініп таралуының, сондай-ақ маусымдық өзгерістердің, заттектердің биоталық айналымдарының және басқа да биологиялық процестердің біршама тұрақтылығы.'),
(90,1, 'Экологиялық фактор', 'кез келген орта жағдайына тіршілік иелерінің бейімделу қабілетімен жауап қайтара алуы.')
		  ON DUPLICATE KEY UPDATE
		  course_id=VALUES(`course_id`), term=VALUES(`term`), definition=VALUES(`definition`)");

    }
}


