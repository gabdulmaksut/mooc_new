<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    public function run()
    {
        DB::delete("DELETE FROM users WHERE id = 1");
        DB::unprepared("INSERT INTO `users` (id, email, password, is_admin, role, is_active)
		  VALUES
                (1, 'admin', '" . Hash::make('111openu201911+a') . "', 1, 3, 1)            
		  ON DUPLICATE KEY UPDATE 
		  id=VALUES(`id`), email=VALUES(`email`), 
		  password=VALUES(`password`), is_admin=VALUES(`is_admin`), is_active=VALUES(`is_active`)");

    }
}


/*
 *
        DB::unprepared("TRUNCATE `terms`");
        DB::unprepared("INSERT INTO `terms` (id, course_id, term, definition)
		  VALUES
                (1, 1, 'Абиотикалық орта', 'өлі табиғаттың барлық факторларының кешені.'),
                (2, 1, 'Абиотикалық факторлар', '(гр. \'a\' – теріс және bіotіkos – тірішілік, өмір) –бейорганикалық ортаның тірі организмдерге жасайтын тікелей немесе жанама әсерлерінің жиынтығы; сыртқы ортаның бейорганикалық, физикалық және химиялық жағдайлары.'),
		  ON DUPLICATE KEY UPDATE
		  number=VALUES(`course_id`), title=VALUES(`term`), classname=VALUES(`definition`)");
 */