<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasColumn('lessons', 'filename')) {
            Schema::table('lessons', function($table) {
                $table->dropColumn('filename');
            });
        }

        if (Schema::hasColumn('lessons', 'document')) {
            Schema::table('lessons', function($table) {        
                $table->dropColumn('document');
            });       
        } 

        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lesson_id');
            $table->text('filename');
            $table->text('path');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('files');
    }
}
