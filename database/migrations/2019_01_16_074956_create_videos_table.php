<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasColumn('lessons', 'video_link')) {
            Schema::table('lessons', function($table) {
                $table->dropColumn('video_link');
            });
        }

        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lesson_id');
            $table->text('link');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videos');
    }
}
