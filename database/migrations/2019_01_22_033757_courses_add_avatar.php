<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CoursesAddAvatar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
         Schema::table('courses', function(Blueprint $table) {

            $table->text('filename');
            $table->integer('category_id');
        });
     }

    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('courses', 'filename')) {
            Schema::table('courses', function($table) {
               $table->dropColumn('filename');
           });
        }
        if (Schema::hasColumn('courses', 'category_id')) {
            Schema::table('courses', function($table) {
               $table->dropColumn('category_id');
           });
        }
        
            
    }
}
