<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasColumn('lessons', 'document')) {
            Schema::table('lessons', function(Blueprint $table) {
                $table->string('document')->default('');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('lessons', 'document')) {
            Schema::table('lessons', function (Blueprint $table) {
                $table->dropColumn('document');
            });
        }
    }
}
