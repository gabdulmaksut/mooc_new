<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {  
     Schema::dropIfExists('lessons'); 
        Schema::create('lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id');
            $table->integer('test_id');
            $table->string('lesson_name')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons');
    }
}
